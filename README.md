# README - Panoramakopf Arduino #

##### Use Arduino IDE 1.5.8 Beta -> http://arduino.cc/download.php?f=/arduino-1.5.8-windows.exe #####

##### Available cameras for PTP (USB) #####
##### Tested Devices includes (+ = ok / - nok) #####

+ 1D Mark III
- (+) 1D Mark IV
+ (-) 5D
- (+) 5D Mark II
+ (+) 7D 
- (-)20D 
+ 30D 
- 40D 
+ 60D 
- 300D 
+ 350D 
- (+) 400D
+ (+) 450D, xSi
- (+) 500D, T1i 
+ (+) 550D, T2i 

Other cameras can be connected over the 3-Pin analog connector

##### Group Members #####
+ Sabine Schneider  HTW Aalen
- Kirill Wetoschkin HTW Aalen
+ Daniel Bauer HTW Aalen

##### Description: https://bitbucket.org/dtec_code/panoramakopf_arduino/downloads #####