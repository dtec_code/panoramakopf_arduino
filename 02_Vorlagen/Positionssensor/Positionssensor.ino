/*
Tool zum Test des Positionssensors mit dem Due-Board

Sensor anschließen, Hochladen und Messwerte über Terminal empfangen.
Der Positionssensor gibt ca. bis 1000 zurück, solange nichts da.
*/

//Positionssensor
#define PIN_POS_SIG A0
#define PIN_POS_EN 52

void setup()
{
  pinMode(PIN_POS_EN,OUTPUT);
  digitalWrite(PIN_POS_EN,HIGH);
  Serial.begin(9600);
  Serial.println("Hello PC");
}

void loop()
{
  delay(500);
  Serial.println(analogRead(PIN_POS_SIG));
}
