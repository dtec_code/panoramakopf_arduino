//------------------------------------------------------------
// Description:
//  Tasks zur steuerung mittels Terminal
//
// Author:
//  Christoph Ilg
//
// Date:
//  24.03.2014
//------------------------------------------------------------


#ifndef TERMINALTASKS_CPP
#define TERMINALTASKS_CPP

#include "terminaltask.h"

void terminalecho()
{
  Serial.println("ECHO");
}

void terminalgetcommand()
{

  if (Serial3.available()>0)
  {
bool ok = false;
while (Serial3.available()>0)
{
if (Serial3.read()=='R') ok = true;
}
if (ok)
{
//starte Motor
    short myadr = mem::newadr(6);
    unsigned char *myptr = mem::getptr(myadr);
    *myptr = 0;
    *(myptr+1) = 1;
    *(myptr+2) = 0;
    *(myptr+3) = 0;
    *(myptr+4) = 2;
    *(myptr+5) = 255;
    messager::put(myadr,0);
    Panoramakopfcontroller::init();
    myadr = mem::newadr(6);
    myptr = mem::getptr(myadr);
    *myptr = 0;
    *(myptr+1) = 2;
    *(myptr+2) = 0;
    *(myptr+3) = 0;
    *(myptr+4) = 2;
    *(myptr+5) = 255;
    messager::put(myadr,0);
    Panoramakopfcontroller::init();
    
}
  }
  
}


#endif
