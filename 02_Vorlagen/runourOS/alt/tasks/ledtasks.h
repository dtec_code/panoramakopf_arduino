//------------------------------------------------------------
// Description:
//  Tasks zur steuerung der LEDs
//
// Author:
//  Christoph Ilg
//
// Date:
//  24.03.2014
//------------------------------------------------------------


#ifndef LEDTASKS_H
#define LEDTASKS_H

#include <Arduino.h>
#include "..\definitions\pindefs.h"


void LEDtoggle_BT();

void LEDtoggle_Status();

void LEDtoggle_Warn();



#endif
