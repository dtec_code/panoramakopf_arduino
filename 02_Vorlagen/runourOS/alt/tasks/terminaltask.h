//------------------------------------------------------------
// Description:
//  Tasks zur steuerung mittels Terminal
//
// Author:
//  Christoph Ilg
//
// Date:
//  24.03.2014
//------------------------------------------------------------


#ifndef TERMINALTASKS_H
#define TERMINALTASKS_H

#include <Arduino.h>
#include "..\Panoramakopfcontroller.h"

void terminalecho();

void terminalgetcommand();


#endif
