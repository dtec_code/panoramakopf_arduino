//------------------------------------------------------------
// Description:
//  Tasks zur steuerung der LEDs
//
// Author:
//  Christoph Ilg
//
// Date:
//  24.03.2014
//------------------------------------------------------------


#ifndef LEDTASKS_CPP
#define LEDTASKS_CPP

#include "ledtasks.h"


void LEDtoggle_BT()
{
  static bool onout = false;
  onout =! onout;
  digitalWrite(PIN_LED_BT,onout);
}

void LEDtoggle_Status()
{
  static bool onout = false;
  onout =! onout;
  digitalWrite(PIN_LED_STATUS,onout);
}

void LEDtoggle_Warn()
{
  static bool onout = false;
  onout =! onout;
  digitalWrite(PIN_LED_WARN,onout);
}


#endif
