//------------------------------------------------------------
// Description:
//  Der Controller f�r den Panoramakopf
//
// Author:
//  Christoph Ilg
//
// Date:
//  24.03.2014
//------------------------------------------------------------


#ifndef PANORAMAKOPFCONTROLLER_H
#define PANORAMAKOPFCONTROLLER_H

#include <Arduino.h>
#include "..\ourOS\functionality\scheduler.h"
#include "..\ourOS\functionality\mem.h"
#include "..\ourOS\functionality\messager.h"
#include "..\ourOS\functionality\eventsys.h"
#include "pindefs.h"

#define PANORAMAKOPFNULLPOS_SCHWELLE 600



class commandprocessor
{
private:
	static unsigned char commanddata[100];
	static unsigned char commandlength;
	static unsigned char command_nowindex;
	static unsigned char machine_reg1;
	static unsigned char machine_reg2;
	static unsigned char machine_reg3;
	static unsigned char machine_reg4;
public:
	//kopiert daten aus Dataptr mit l�nge datalength in commanddata
	static void fillcontroller(unsigned char *dataptr, unsigned char datalength);
	//gibt genau dann true zur�ck, wenn finished, false wenn etwas zu tun bleibt
	static bool process();
};



class Panoramakopfcontroller
{
private:
	//zeigt an, ob die Arbeit getan ist
	static bool workisdone;
	//gibt aktuellen steuerbefehl an
	static unsigned char steuerbefehl;
	//zeigt aktuelles Kommando an, das abgearbeitet wird
	static unsigned char kommandotodo;
	//gibt an, ob arbeit anzuhalten ist
	//
	//gibt an, wie viele Schritte der Motor noch zu drehen ist
	static unsigned short motorstepstodo;
	//l�st Kamera aus - Funktionalit�t noch implementieren (USB?)

	//eventfunktion als timer
	static void eventtimer();
public:

	//Initialisiert den Panoramakopfcontroller
	static void init();
	//dient als Task, der ausgef�hrt wird um zu kontrollieren?
	static void run();
	//dient als Task f�r die Motoren, Taskzeit z.B. 500
	static void Motorrun();

	//leitet warten auf die eingegebene Zeit in 0,1 sec bevor workfinish ein
	static void c_wait(unsigned char time);
	//leitet finden der Nullposition ein
	static void c_findzero();
	//dreht Motor mit Nullposition steps Schritte in direction
	static void c_motorNull(unsigned short steps, bool direction);
	//dreht anderen Motor steps Schritte in direction
	static void c_motor(unsigned short steps, bool direction);
	//l�se Kamera aus, z.B. �ber USB - ist noch Dummy!
	static void c_capture();
};



#endif
