//------------------------------------------------------------
// Description:
//  Eventsystem
//
// Author:
//  Christoph Ilg
//
// Date:
//  24.03.2014
//------------------------------------------------------------


#ifndef EVENTSYS_CPP
#define EVENTSYS_CPP

#include "eventsys.h"


VoidFnct eventsys::fktptr[EVENTS_MAXNUMBER];
unsigned short eventsys::eventdoctr[EVENTS_MAXNUMBER];

void eventsys::init()
{
	for (unsigned char i = 0; i < EVENTS_MAXNUMBER; i++)
	{
		eventdoctr[i] = 0;
		fktptr[i] = SPECIFICNULLPTR;
	}
}

void eventsys::attach(unsigned char eventname, VoidFnct todo)
{
	fktptr[eventname] = todo;
}

void eventsys::raise_now(unsigned char eventname)
{
	if (fktptr[eventname] != SPECIFICNULLPTR) (fktptr[eventname])();
}

void eventsys::raise_task(unsigned char eventname, short raisectr)
{
	eventdoctr[eventname] = raisectr;
}

void eventsys::run_highprio()
{
	for (unsigned char i = 0; i < EVENTS_MAXNUMBER; i++)
	{
		if (eventdoctr[i] <= 0) continue;
		eventdoctr[i]--;
		if (eventdoctr[i] == 0) (fktptr[i])();
	}
}

void eventsys::run_lowprio()
{
	for (unsigned char i = 0; i < EVENTS_MAXNUMBER; i++)
	{
		if (eventdoctr[i] >= 0) continue;
		eventdoctr[i]++;
		if (eventdoctr[i] == 0) (fktptr[i])();
	}
}

#endif
