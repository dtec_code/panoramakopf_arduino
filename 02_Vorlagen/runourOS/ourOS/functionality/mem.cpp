//------------------------------------------------------------
// Description:
//  Die Speicherverwaltung
//
// Author:
//  Christoph Ilg
//
// Date:
//  24.03.2014
//------------------------------------------------------------


#ifndef MEM_CPP
#define MEM_CPP

#include "mem.h"


	//Die Datenbl�cke
	DATATYP mem::datasmall[NUMBER_SMALL][LENGTH_SMALL];
	DATATYP mem::datamedium[NUMBER_MEDIUM][LENGTH_MEDIUM];
	DATATYP mem::databig[NUMBER_BIG][LENGTH_BIG];
	//Belegte L�nge der Datenbl�cke, bei 0 leer
	DATATYP_LENGTH mem::lengthsmall[NUMBER_SMALL];
	DATATYP_LENGTH mem::lengthmedium[NUMBER_MEDIUM];
	DATATYP_LENGTH mem::lengthbig[NUMBER_BIG];
	//niedrigster freie Adresse f�r eine L�nge
	unsigned short mem::lowestsmall;
	unsigned short mem::lowestmedium;
	unsigned short mem::lowestbig;


	void mem::meminit()
	{
		for (unsigned short i = 0; i < NUMBER_SMALL; i++)
		{
			lengthsmall[i] = 0;
		}
		for (unsigned short i = 0; i < NUMBER_MEDIUM; i++)
		{
			lengthmedium[i] = 0;
		}
		for (unsigned short i = 0; i < NUMBER_BIG; i++)
		{
			lengthbig[i] = 0;
		}
		lowestsmall = 0;
		lowestmedium = NUMBER_SMALL;
		lowestbig = NUMBER_SMALL + NUMBER_MEDIUM;
	}


	short mem::newadr(DATATYP_LENGTH length)
	{
                INTERRUPT_DISABLE_METHOD
		if ((length < (LENGTH_SMALL + 1)) && (lowestsmall < NUMBER_SMALL))
		{
			unsigned short lowestsmallold = lowestsmall;
			lowestsmall++;
			while (lowestsmall < NUMBER_SMALL)
			{
				if (lengthsmall[lowestsmall] == 0) break;
				lowestsmall++;
			}
			lengthsmall[lowestsmallold] = length;
                        INTERRUPT_ENABLE_METHOD
			return lowestsmallold;
		}
		if ((length < (LENGTH_MEDIUM + 1)) && (lowestmedium < NUMBER_SMALL + NUMBER_MEDIUM))
		{
			unsigned short lowestmediumold = lowestmedium;
			lowestmedium++;
			while (lowestmedium < NUMBER_SMALL + NUMBER_MEDIUM)
			{
				if (lengthmedium[lowestmedium - NUMBER_SMALL] == 0) break;
				lowestmedium++;
			}
			lengthmedium[lowestmediumold - NUMBER_SMALL] = length;
                        INTERRUPT_ENABLE_METHOD
			return lowestmediumold;
		}
		if ((length < (LENGTH_BIG + 1)) && (lowestbig < NUMBER_SMALL + NUMBER_MEDIUM + NUMBER_BIG))
		{
			unsigned short lowestbigold = lowestbig;
			lowestbig++;
			while (lowestbig < NUMBER_SMALL + NUMBER_MEDIUM + NUMBER_BIG)
			{
				if (lengthbig[lowestbig - NUMBER_SMALL - NUMBER_MEDIUM] == 0) break;
				lowestbig++;
			}
			lengthbig[lowestbigold - NUMBER_SMALL - NUMBER_MEDIUM] = length;
                        INTERRUPT_ENABLE_METHOD
			return lowestbigold;
		}
                INTERRUPT_ENABLE_METHOD
		return -1;
	}


	void mem::freeadr(unsigned short adress)
	{
                INTERRUPT_DISABLE_METHOD
		if (adress < NUMBER_SMALL)
		{
			lengthsmall[adress] = 0;
			if (adress < lowestsmall) lowestsmall = adress;
                        INTERRUPT_ENABLE_METHOD
			return;
		}
		if (adress < NUMBER_SMALL + NUMBER_MEDIUM)
		{
			lengthmedium[adress - NUMBER_SMALL] = 0;
			if (adress < lowestmedium) lowestmedium = adress;
                        INTERRUPT_ENABLE_METHOD
			return;
		}
		if (adress < NUMBER_SMALL + NUMBER_MEDIUM + NUMBER_BIG)
		{
			lengthbig[adress - NUMBER_SMALL - NUMBER_MEDIUM] = 0;
			if (adress < lowestbig) lowestbig = adress;
                        INTERRUPT_ENABLE_METHOD
			return;
		}
                INTERRUPT_ENABLE_METHOD
	}
	

	DATATYP_LENGTH mem::getadr(unsigned short adress)
	{
                INTERRUPT_DISABLE_METHOD
		if (adress < NUMBER_SMALL)
		{
                        INTERRUPT_ENABLE_METHOD
			return lengthsmall[adress];
		}
		if (adress < NUMBER_SMALL + NUMBER_MEDIUM)
		{
                        INTERRUPT_ENABLE_METHOD
			return lengthmedium[adress - NUMBER_SMALL];
		}
		if (adress < NUMBER_SMALL + NUMBER_MEDIUM + NUMBER_BIG)
		{
                        INTERRUPT_ENABLE_METHOD
			return lengthbig[adress - NUMBER_SMALL - NUMBER_MEDIUM];
		}
                INTERRUPT_ENABLE_METHOD
		return 0;
	}


        bool mem::shortenlength(unsigned short adress, DATATYP_LENGTH newlength)
        {
                if (adress < NUMBER_SMALL)
		{
                        if (lengthsmall[adress] >= newlength)
                        {
                            lengthsmall[adress] = newlength;
                            return true;
                        }
                        return false;
		}
		if (adress < NUMBER_SMALL + NUMBER_MEDIUM)
		{
                        if (lengthmedium[adress - NUMBER_SMALL] >= newlength)
                        {
                            lengthmedium[adress - NUMBER_SMALL] = newlength;
                            return true;
                        }
			return false;
		}
		if (adress < NUMBER_SMALL + NUMBER_MEDIUM + NUMBER_BIG)
		{
                        if (lengthbig[adress - NUMBER_SMALL - NUMBER_MEDIUM] >= newlength)
                        {
                            lengthbig[adress - NUMBER_SMALL - NUMBER_MEDIUM] = newlength;
                            return true;
                        }
			return false;
		}
                return false;
        }


	DATATYP *mem::getptr(unsigned short adress)
	{
                INTERRUPT_DISABLE_METHOD
		if (adress < NUMBER_SMALL)
		{
                        INTERRUPT_ENABLE_METHOD
			return datasmall[adress];
		}
		if (adress < NUMBER_SMALL + NUMBER_MEDIUM)
		{
                        INTERRUPT_ENABLE_METHOD
			return datamedium[adress - NUMBER_SMALL];
		}
		if (adress < NUMBER_SMALL + NUMBER_MEDIUM + NUMBER_BIG)
		{
                        INTERRUPT_ENABLE_METHOD
			return databig[adress - NUMBER_SMALL - NUMBER_MEDIUM];
		}
                INTERRUPT_ENABLE_METHOD
		return SPECIFICNULLPTR; //null bzw. NULL
	}


#endif
