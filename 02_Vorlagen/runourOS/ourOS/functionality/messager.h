//------------------------------------------------------------
// Description:
//  Der Messager
//
// Author:
//  Christoph Ilg
//
// Date:
//  24.03.2014
//------------------------------------------------------------


#ifndef MESSAGER_H
#define MESSAGER_H




#include "..\HWspecific.h"
#include "..\MESSAGERdefs.h"

class messager
{
private:
	static unsigned short readctr[CHANNELS];
	static unsigned short writectr[CHANNELS];
	static bool full[CHANNELS];
	static MESSAGEDATATYP data[CHANNELS][MAXMESSAGES];
public:
	static void messagerinit();
	//gibt eine message in channel zum Abruf bereit,
	//gibt genau dann true zur�ck wenn erfolgreich
        //ist sicher f�r die Verwendung zur Kommunikation
        //mit Interrupts!!
	static bool put(MESSAGEDATATYP inmessage, unsigned char channel);
	//holt die nachricht aus channel,
	//gibt genau dann true zur�ck wenn erfolgreich
        //ist sicher f�r die Verwendung zur Kommunikation
        //mit Interrupts!!
	static bool get(MESSAGEDATATYP *outmessage, unsigned char channel);
};

#endif

