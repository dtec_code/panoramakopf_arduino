//------------------------------------------------------------
// Description:
//  Die Definition der Eventnamen
//
// Author:
//  Christoph Ilg
//
// Date:
//  24.03.2014
//------------------------------------------------------------

#ifndef EVENTSDEFS_H
#define EVENTSDEFS_H


//die maximale Anzahl an Events, die zur Verf�gung stehen
#define EVENTS_MAXNUMBER 4

//benutzerdefinierte Definitionen
#define EVENT_PKCONTROLLERTIMER 0
#define EVENT_DUMMY2 1
#define EVENT_DUMMY3 2
#define EVENT_DUMMY4 3


#endif