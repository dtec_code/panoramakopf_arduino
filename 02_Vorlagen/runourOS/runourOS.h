//------------------------------------------------------------
// Description:
//  Das Interface zum starten der Software
//
// Author:
//  Christoph Ilg
//
// Date:
//  24.03.2014
//------------------------------------------------------------


#ifndef RUNOUROS_H
#define RUNOUROS_H

#include <Arduino.h>
#include "ourOS\functionality\scheduler.h"
#include "ourOS\functionality\mem.h"
#include "ourOS\functionality\messager.h"
#include "ourOS\functionality\eventsys.h"
#include "hardware\pindefs.h"
#include "hardware\Panoramakopfcontroller.h"
#include "hardware\BTtask.h"


void writegetcommand(String command)
{
  digitalWrite(PIN_BT_RTS,LOW);
  delay(10);
  while (digitalRead(PIN_BT_CTS)==HIGH) delay(10);
  Serial3.print("\r\n"+command+"\r\n");
  delay(10);
  digitalWrite(PIN_BT_RTS,HIGH);  
}



//startet die Software
void runourOS()
{


	//hier noch irgendwo goto restart bzw. goto fail rein tun!!!
	//-> im Fehlerfall neustart (Interrupt mit kritischen Flags)

	//auch noch einen Fall bei Tiefentladung!
	//-> nur noch warn LED blinken lassen

	//debugtest
	Serial.begin(9600);
	Serial.println("hello PC");
	delay(1000);
	//Pins initialisieren
	//Positionssensor
	//...
	//Optokoppler Kameraverbindung
	//...
	//LEDs
	pinMode(PIN_LED_STATUS, OUTPUT);
	pinMode(PIN_LED_BT, OUTPUT);
	pinMode(PIN_LED_WARN, OUTPUT);
	digitalWrite(PIN_LED_STATUS, LOW);
	digitalWrite(PIN_LED_BT, LOW);
	digitalWrite(PIN_LED_WARN, LOW);
	//Motor 1
	pinMode(PIN_M1_EN, OUTPUT);
	pinMode(PIN_M1_DIR, OUTPUT);
	pinMode(PIN_M1_CLK, OUTPUT);
	pinMode(PIN_M1_ABSENK, INPUT);
	digitalWrite(PIN_M1_EN, HIGH);
	digitalWrite(PIN_M1_DIR, LOW);
	digitalWrite(PIN_M1_CLK, LOW);
	//Motor 2
	pinMode(PIN_M2_EN, OUTPUT);
	pinMode(PIN_M2_DIR, OUTPUT);
	pinMode(PIN_M2_CLK, OUTPUT);
	pinMode(PIN_M2_ABSENK, INPUT);
	digitalWrite(PIN_M2_EN, HIGH);
	digitalWrite(PIN_M2_DIR, LOW);
	digitalWrite(PIN_M2_CLK, LOW);
	//Positionssensor
	pinMode(PIN_POS_EN, OUTPUT);
	digitalWrite(PIN_POS_EN, LOW);
	//Bluetooth
	Serial3.begin(38400);
	Serial3.flush();
  	//standardpinpegel BT setzen
  	pinMode(PIN_BT_CTS,INPUT); //CTS geht rein
  	pinMode(PIN_BT_RTS,INPUT); //RTS geht rein


	//initialisiere messager
	messager::messagerinit();
	//initialisiere mem
	mem::meminit();
	//initialisiere eventsys
	eventsys::init();

    //initialisiere Panoramakopfcontroller
    Panoramakopfcontroller::init();
	//initialisere Bluetooth
	BTtask::init();


	//starte scheduler!!!
	Scheduler::run();
}

//extern void runilgOS();

//Dies ist bei Arduino notwendig wenn Dateien in Unterordner
#include "ourOS\functionality\messager.cpp"
#include "ourOS\functionality\mem.cpp"
#include "ourOS\functionality\scheduler.cpp"
#include "ourOS\functionality\eventsys.cpp"
//#include "tasks\ledtasks.cpp"
//#include "tasks\terminaltask.cpp"
#include "hardware\BTtask.cpp"
#include "hardware\Panoramakopfcontroller.cpp"
#include "library\stringlib.cpp"

#endif
