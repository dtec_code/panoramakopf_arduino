/*
Tool zum Test der Motorenansteuerung mit Due und Shield

Motoren anschließen, Hochladen und diese sollten sich drehen
*/

#define SMC_1_ENABLE 30
#define SMC_1_DIR 32
#define SMC_1_CLK 2
#define SMC_2_ENABLE 36
#define SMC_2_DIR 38
#define SMC_2_CLK 3

void setup()
{
  //Modi setzen
  pinMode(SMC_1_ENABLE,OUTPUT);
  pinMode(SMC_1_DIR,OUTPUT);
  pinMode(SMC_1_CLK,OUTPUT);
  pinMode(SMC_2_ENABLE,OUTPUT);
  pinMode(SMC_2_DIR,OUTPUT);
  pinMode(SMC_2_CLK,OUTPUT);
  //enable
  digitalWrite(SMC_1_ENABLE,LOW);
  digitalWrite(SMC_2_ENABLE,LOW);
  //setze direction
  digitalWrite(SMC_1_DIR,HIGH);
  digitalWrite(SMC_2_DIR,HIGH);
  //Serial
  Serial.begin(9600);
  Serial.println("hello i am ready");
}

void loop()
{
  static boolean lvl = true;
  static boolean lvl2 = true;
  lvl = !lvl;
  digitalWrite(SMC_1_CLK,lvl);
  digitalWrite(SMC_2_CLK,lvl); mache dies noch erheblich langsamer
  delayMicroseconds(100);
  static int ctr = 0;
  ctr++;
  if (ctr==12000)
  {
    lvl2=!lvl2;
//  digitalWrite(SMC_1_DIR,lvl2);
  ctr=0;
  }
}
