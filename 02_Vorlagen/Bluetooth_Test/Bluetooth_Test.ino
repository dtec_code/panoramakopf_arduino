/*
  Tool zum Test des Bluetooth-Moduls auf dem Due-Board
  
  Hochladen, Terminal öffnen und Befehle senden und empfangen
  
  z.B. +INQ=1 senden zum Starten des Suchvorganges,
  die Befehle sind im Datenblatt zu finden
*/

//Definitionen
#define PIN_BT_RESET 48
#define PIN_BT_TX 14
#define PIN_BT_RX 15
#define PIN_BT_CTS 24
#define PIN_BT_RTS 26
#define PIN_BT_DISCONNECT 22

void setup() {
  pinMode(PIN_BT_RESET,OUTPUT);
  //pinMode(PIN_BT_TX,OUTPUT);
  //pinMode(PIN_BT_RX,INPUT); 
  pinMode(PIN_BT_CTS,INPUT); //CTS geht rein, setze auf wenn
  pinMode(PIN_BT_RTS,INPUT); //RTS geht raus, setze auf wenn
  pinMode(PIN_BT_DISCONNECT,INPUT); 
  
  Serial.begin(9600);
  Serial3.begin(38400);
  
  //standardpinpegel setzen
 // digitalWrite(PIN_BT_DISCONNECT,LOW); //disconnecten mit high-flanke
 // digitalWrite(PIN_BT_RESET,HIGH); //LOW zum reseten 
 // digitalWrite(PIN_BT_RTS,LOW); //
  
  Serial.println("Hello PC");
   
  Serial.println("command to send?"); 
  //set device as slave
 // writegetcommand("+STWMOD=0");
  //start inquiring
 // writegetcommand("+INQ=1");
}



void writegetcommand(String command)
{
 // digitalWrite(PIN_BT_RTS,HIGH);
  delay(10);
 // if (digitalRead(PIN_BT_CTS)==HIGH) Serial.println("CTS not ready to send...");
 // while (digitalRead(PIN_BT_CTS)==HIGH) delay(10);
  Serial.println("CTS ready to send");
  Serial.println("command: "+command+" was send");
  Serial3.print("\r\n"+command+"\r\n");
  delay(10);
  //digitalWrite(PIN_BT_RTS,LOW);  
  
  delay(1000);
  if (Serial3.available()>0)
  {
    Serial.println("received: ");
    char text;
    while(Serial3.available()>0) 
    {
      text = Serial3.read();
      Serial.print(text);
    }
    Serial.print("\r\n");
  }
  else
    Serial.println("nothing received");
}
  

void loop() 
{
    delay(100);
    if (Serial.available()>0)
    {
       char text;
       String com="";
       while(Serial.available()>0) 
       {
         text = Serial.read();
         com+=text;
       }
       writegetcommand(com);
       Serial.println("command to send?");
    }
    delay(100);
  if (Serial3.available()>0)
  {
    Serial.println("received: ");
    char text;
    while(Serial3.available()>0) 
    {
      text = Serial3.read();
      Serial.print(text);
    }
    Serial.print("\r\n");
  }
}
