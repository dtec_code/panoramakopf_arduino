/* 
 * File:   startdefines.h
 * Author: Christoph Ilg
 *
 * Created on 23. M�rz 2014, 11:04
 */

#ifndef STARTDEFINES_H
#define	STARTDEFINES_H

// configuration bit settings, Fcy=80MHz, Fpb=10MHz
#pragma config POSCMOD=XT, FNOSC=PRIPLL
#pragma config FPLLIDIV=DIV_2, FPLLMUL=MUL_20, FPLLODIV=DIV_1
#pragma config FPBDIV=DIV_4, FWDTEN=OFF, CP=OFF, BWP=OFF

#endif	/* STARTDEFINES_H */

