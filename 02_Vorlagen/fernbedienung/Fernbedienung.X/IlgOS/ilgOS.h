//------------------------------------------------------------
// Description:
//  Das Interface zum starten der Software
//
// Author:
//  Christoph Ilg
//
// Date:
//  24.03.2014
//------------------------------------------------------------


#ifndef ILGOS_H
#define ILGOS_H


#include "../startdefines.h"
#include <cstdlib>
#include <GenericTypeDefs.h>
#include <p32xxxx.h>
#include <plib.h>
#include "../Display/fonts.h"
#include "../Display\HX8347D_driver.h"
#include "../Display\draw_interface.h"
#include "../Display\touch_driver.h"
#include "../GUI/STDwindow.h"
#include "../GUI/GUItask.h"
#include "../Bluetooth/MYUART.h"
#include "scheduler.h"
#include "mem.h"
#include "messager.h"
#include "../Bluetooth/BTtask.h"

//startet die Software
void runilgOS()
{


	//hier noch irgendwo goto restart bzw. goto fail rein tun!!!
	//-> im Fehlerfall neustart (Interrupt mit kritischen Flags)

	//auch noch einen Fall bei Tiefentladung!
	//-> nur noch warn LED blinken lassen

        //setze LED-Pins
        mPORTGSetPinsDigitalOut(BIT_14);
        mPORTGSetBits(BIT_14);

        DDPCONbits.JTAGEN = 0;	// jtag port disabled
	INTEnableSystemMultiVectoredInt();

        //initialisiere Display
	_LCD_BKL = 0;
	initLCD();
	touchInit();
	_LCD_BKL = 1;

        //l�sche Displayinhalt noch tun

	//initialisiere messager
	messager::messagerinit();
	//initialisiere mem
	mem::meminit();
        //initialisiere UART
        UARTinit();
        //initialisiere BTtask
        BTtask::init();
	//starte scheduler!!!
	Scheduler::run();
}

#endif
