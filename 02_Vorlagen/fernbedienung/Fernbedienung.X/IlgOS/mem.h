//------------------------------------------------------------
// Description:
//  Die Speicherverwaltung
//
// Author:
//  Christoph Ilg
//
// Date:
//  24.03.2014
//------------------------------------------------------------


#ifndef MEM_H
#define MEM_H

#include "HWspecific.h"

#define DATATYP unsigned char
#define DATATYP_LENGTH unsigned char
#define LENGTH_SMALL 16
#define NUMBER_SMALL 128
#define LENGTH_MEDIUM 64
#define NUMBER_MEDIUM 64
#define LENGTH_BIG 256
#define NUMBER_BIG 32


class mem
{
private:
	//Die Datenbl�cke
	static DATATYP datasmall[NUMBER_SMALL][LENGTH_SMALL];
	static DATATYP datamedium[NUMBER_MEDIUM][LENGTH_MEDIUM];
	static DATATYP databig[NUMBER_BIG][LENGTH_BIG];
	//Belegte L�nge der Datenbl�cke, bei 0 leer
	static DATATYP_LENGTH lengthsmall[NUMBER_SMALL];
	static DATATYP_LENGTH lengthmedium[NUMBER_MEDIUM];
	static DATATYP_LENGTH lengthbig[NUMBER_BIG];
	//niedrigster freie Adresse f�r eine L�nge
	static unsigned short lowestsmall;
	static unsigned short lowestmedium;
	static unsigned short lowestbig;
public:
	//zur Initialisierung bzw. dem neuaufsetzen des Speichers
	static void meminit();
	//gibt eine Adresse f�r die gew�nschte L�nge zur�ck, -1
	//wenn nicht erfolgreich, der Bereich wird automatisch
	//f�r diese L�nge belegt
        //Diese Vergabe ist sicher f�r die Verwendung zur Kommunikation
        //mit Interrupts!!
	static short newadr(DATATYP_LENGTH length);
	//befreit die entsprechende Adresse und gibt sie f�r
	//eine neue freigabe frei!
        //Diese Freigabe ist sicher f�r die Verwendung zur Kommunikation
        //mit Interrupts!!
	static void freeadr(unsigned short adress);
	//gibt L�nge f�r eine Adresse zur�ck, 0 falls nichts da bzw.
	//reserviert
        //ist sicher f�r die Verwendung zur Kommunikation
        //mit Interrupts!!
	static DATATYP_LENGTH getadr(unsigned short adress);
        //k�rzt die L�nge dieses memblocks
        //(Achtung: kein verl�ngern m�glich!)
        static bool shortenlength(unsigned short adress, DATATYP_LENGTH newlength);
	//gibt Pointer zu der Adresse zur�ck, null falls nichts da
        //ist sicher f�r die Verwendung zur Kommunikation
        //mit Interrupts!!
        //Achtung -> was dann geschrieben wird nicht mehr sicher
        //daher erst Daten schreiben, dann Adresse versenden
	static DATATYP *getptr(unsigned short adress);
};


#endif
