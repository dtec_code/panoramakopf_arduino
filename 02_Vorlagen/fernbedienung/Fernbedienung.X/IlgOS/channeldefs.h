//------------------------------------------------------------
// Description:
//  Die Definition der Messagekan�le
//
// Author:
//  Christoph Ilg
//
// Date:
//  24.03.2014
//------------------------------------------------------------

#ifndef CHANNELDEFS_H
#define CHANNELDEFS_H


//Scheduler
#define CHANNELFORSCHEDULER 0
#define CHANNELFORUART 1
#define CHANNELFORBT_TOSEND 2//Kanal, an den die zu sendenden BT-Nachrichten kommen
#define CHANNELFORBT_GUIDATAIN 3 //Kanal, der die BT-Nachrichtendaten f�r die GUI

#endif