//------------------------------------------------------------
// Description:
//  Der Messager
//
// Author:
//  Christoph Ilg
//
// Date:
//  24.03.2014
//------------------------------------------------------------


#ifndef MESSAGER_CPP
#define MESSAGER_CPP

#include "messager.h"


unsigned short messager::readctr[CHANNELS];
unsigned short messager::writectr[CHANNELS];
bool messager::full[CHANNELS];
MESSAGEDATATYP messager::data[CHANNELS][MAXMESSAGES];


void messager::messagerinit()
	{
		for (long i = 0; i < CHANNELS; i++)
		{
			writectr[i] = 0;
			readctr[i] = 0;
			full[i] = false;
		}
	}


bool messager::put(MESSAGEDATATYP inmessage, unsigned char channel)
	{
#if DEBUG == 1
		if (channel >= CHANNELS) return false;
#endif
		if (full[channel]) return false;
                INTERRUPT_DISABLE_METHOD
		data[channel][writectr[channel]] = inmessage;
		if (writectr[channel] == MAXMESSAGES - 1)
			writectr[channel] = 0;
		else
			writectr[channel]++;
		if (writectr[channel] == readctr[channel]) full[channel] = true;
                INTERRUPT_ENABLE_METHOD
		return true;
	}


bool messager::get(MESSAGEDATATYP *outmessage, unsigned char channel)
	{
#if DEBUG == 1
		if (channel >= CHANNELS) 
			return false;
#endif
                INTERRUPT_DISABLE_METHOD
		if (full[channel])
			full[channel] = false;
		else if (writectr[channel] == readctr[channel])
                {
                        INTERRUPT_ENABLE_METHOD
			return false;
                }
		*outmessage = data[channel][readctr[channel]];
		if (readctr[channel] == MAXMESSAGES - 1)
			readctr[channel] = 0;
		else
			readctr[channel]++;
                INTERRUPT_ENABLE_METHOD
                return true;
	}


#endif
