//------------------------------------------------------------
// Description:
//  Hardwarespezifische Definitionen (z.B. arduino vs 
//  Mikromedia)
//
// Author:
//  Christoph Ilg
//
// Date:
//  24.03.2014
//------------------------------------------------------------

#ifndef HWSPECIFIC_H
#define HWSPECIFIC_H

#include <plib.h>

//Methode, um die aktuelle Zeit zu bekommen
//sowie evtl. Multiplier
#define MIKROSECONDSMULTIPLIER 80 //80 bei Mikromedia
#define GETNOWTIME ReadCoreTimer();
#define INTERRUPT_ENABLE_METHOD INTEnableInterrupts();
#define INTERRUPT_DISABLE_METHOD INTDisableInterrupts();

//Nullpointer
#define SPECIFICNULLPTR 0 //null
#endif