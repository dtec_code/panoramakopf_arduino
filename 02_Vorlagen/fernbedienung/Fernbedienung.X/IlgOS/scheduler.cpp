//------------------------------------------------------------
// Description:
//  Der Scheduler, die auszuf�hrenden Tasks im cpp-file
//  festlegen
//
// Author:
//  Christoph Ilg
//
// Date:
//  24.03.2014
//------------------------------------------------------------


#ifndef SCHEDULER_CPP
#define SCHEDULER_CPP

#include "scheduler.h"

	unsigned long Scheduler::tasktimes_lowprio[NUMBEROFTASKS_LOWPRIO];
	unsigned long Scheduler::tasktimes_highprio[NUMBEROFTASKS_HIGHPRIO];
	unsigned long Scheduler::nexttime_lowprio[NUMBEROFTASKS_LOWPRIO];
	unsigned long Scheduler::nexttime_highprio[NUMBEROFTASKS_HIGHPRIO];
	//die aktuelle Zeit
	unsigned long Scheduler::nowtime;
	//nexttask-Variablen
	unsigned char Scheduler::nexttask_highprio;
	unsigned char Scheduler::nexttask_lowprio;
	unsigned long Scheduler::nexttasktime_highprio;
	unsigned long Scheduler::nexttasktime_lowprio;


	inline void Scheduler::runtask_lowprio(unsigned char taskindex)
	{
		switch (taskindex)
		{
        //-------------------------------------------
        //f�ge hier die Taskaufrufe nach Indizes ein
        //-------------------------------------------
		case 0:
                    LEDtoggletask();
                    break;
		case 1:
                    BTtask::run();
                    break;
		//-------------------------------------------
		}
	}


	inline void Scheduler::runtask_highprio(unsigned char taskindex)
	{
		switch (taskindex)
		{
			//-------------------------------------------
			//f�ge hier die Taskaufrufe nach Indizes ein
			//-------------------------------------------
		case 0:
                    GUItask::run();
                    break;
			//-------------------------------------------
		}
	}
	

	inline void Scheduler::runtask_once(unsigned char taskindex)
	{
		switch (taskindex)
		{
			//-------------------------------------------
			//f�ge hier die Taskaufrufe nach Indizes ein
			//-------------------------------------------
		case 0:
			//...
			break;
			//-------------------------------------------
		}
	}


	inline void Scheduler::actualize_nowtime()
	{
		unsigned long oldtime = nowtime;
		nowtime = GETNOWTIME
		if (oldtime > nowtime) zeitueberlauf();
	}
	

	void Scheduler::zeitueberlauf()
	{
		for (unsigned char i = 0; i < NUMBEROFTASKS_HIGHPRIO; i++)
		{
			if (nexttime_highprio[i] > 2 ^ 31) nexttime_highprio[i] = 0;
		}
		for (unsigned char i = 0; i < NUMBEROFTASKS_LOWPRIO; i++)
		{
			if (nexttime_lowprio[i] > 2 ^ 31) nexttime_lowprio[i] = 0;
		}
		if (nexttasktime_highprio > 2 ^ 31) nexttasktime_highprio = 0;
		if (nexttasktime_lowprio > 2 ^ 31) nexttasktime_lowprio = 0;
	}


	inline bool Scheduler::check_highprio()
	{
		if (nexttasktime_highprio <= nowtime)
		{
			runtask_highprio(nexttask_highprio);
			nexttime_highprio[nexttask_highprio] += tasktimes_highprio[nexttask_highprio];
			nexttasktime_highprio = nexttime_highprio[nexttask_highprio];
			for (unsigned char i = 0; i < NUMBEROFTASKS_HIGHPRIO; i++)
			{
				if (nexttime_highprio[i] < nexttasktime_highprio)
				{
					if (tasktimes_highprio[i] == 0) continue;
					nexttasktime_highprio = nexttime_highprio[i];
					nexttask_highprio = i;
				}
			}
			return true;
		}
		return false;
	}
	

	inline bool Scheduler::check_messages()
	{
		MESSAGEDATATYP result;
		if (messager::get(&result,CHANNELFORSCHEDULER))
		{
			if ((result < 0) && (result > -257))
			{
				runtask_once(-(result+1));
			}
			else if (result > 0)
			{
				if (mem::getadr(result) == 6)
				{
					unsigned char *myptr = mem::getptr(result);
					if (*myptr == 0)
					{
						if (*(myptr + 1) < NUMBEROFTASKS_HIGHPRIO)
						{
							tasktimes_highprio[*(myptr + 1)] = *(myptr + 2) * 16777216 + *(myptr + 3) * 65536 + *(myptr + 4) *256 + *(myptr + 5);
							tasktimes_highprio[*(myptr + 1)] *= MIKROSECONDSMULTIPLIER;
							nexttime_highprio[*(myptr + 1)] = nowtime + tasktimes_highprio[*(myptr + 1)];
						}
					}
					else if (*myptr == 1)
					{
						if (*(myptr + 1) < NUMBEROFTASKS_LOWPRIO)
						{
							tasktimes_lowprio[*(myptr + 1)] = *(myptr + 2) * 16777216 + *(myptr + 3) * 65536 + *(myptr + 4) *256 + *(myptr + 5);
							tasktimes_lowprio[*(myptr + 1)] *= MIKROSECONDSMULTIPLIER;
                                                        nexttime_lowprio[*(myptr + 1)] = nowtime + tasktimes_lowprio[*(myptr + 1)];
						}
					}
					mem::freeadr(result);
				}
			}
			return true;
		}
		return false;
	}


	inline bool Scheduler::check_lowprio()
	{
		if (nexttasktime_lowprio <= nowtime)
		{
			runtask_lowprio(nexttask_lowprio);
			nexttime_lowprio[nexttask_lowprio] += tasktimes_lowprio[nexttask_lowprio];
			nexttasktime_lowprio = nexttime_lowprio[nexttask_lowprio];
			for (unsigned char i = 0; i < NUMBEROFTASKS_LOWPRIO; i++)
			{
				if (nexttime_lowprio[i] < nexttasktime_lowprio)
				{
					if (tasktimes_lowprio[i] == 0) continue;
					nexttasktime_lowprio = nexttime_lowprio[i];
					nexttask_lowprio = i;
				}
			}
			return true;
		}
		return false;
	}


	void Scheduler::run()
	{
		//Initsequenz
		//-------------------------------------------
		//f�ge hier die Taskwiederholzeiten ein [mus]
		//trage null ein, um den Task erstmal zu 
		//deaktivieren! (ACHTUNG: die nullten Tasks
		//lassen sich jeweils nicht deaktivieren)
		//-------------------------------------------
		tasktimes_lowprio[0] = 500000;
		tasktimes_lowprio[1] = 20000;
		tasktimes_lowprio[2] = 0;
		tasktimes_lowprio[3] = 0;
		tasktimes_highprio[0] = 100000;
		tasktimes_highprio[1] = 0;
		tasktimes_highprio[2] = 0;
		tasktimes_highprio[3] = 0;
		//-------------------------------------------
		for (unsigned char i = 0; i < NUMBEROFTASKS_HIGHPRIO; i++)
		{
			tasktimes_highprio[i] *= MIKROSECONDSMULTIPLIER;
		}
		for (unsigned char i = 0; i < NUMBEROFTASKS_LOWPRIO; i++)
		{
			tasktimes_lowprio[i] *= MIKROSECONDSMULTIPLIER;
		}
		nowtime = GETNOWTIME
		nexttask_highprio = 0;
		nexttask_lowprio = 0;
		nexttasktime_highprio = nowtime + tasktimes_highprio[0];
		nexttasktime_lowprio = nowtime + tasktimes_lowprio[0];
		unsigned char nexttask_lowprio = nowtime + tasktimes_highprio[0];
		for (unsigned char i = 0; i < NUMBEROFTASKS_HIGHPRIO; i++)
		{
			nexttime_highprio[i] = nowtime + tasktimes_highprio[i];
			if (nexttime_highprio[i] < nexttasktime_highprio)
			{
				if (tasktimes_highprio[i] == 0) continue;
				nexttasktime_highprio = nexttime_highprio[i];
				nexttask_highprio = i;
			}
		}
		for (unsigned char i = 0; i < NUMBEROFTASKS_LOWPRIO; i++)
		{
			nexttime_lowprio[i] = nowtime + tasktimes_lowprio[i];
			if (nexttime_lowprio[i] < nexttasktime_lowprio)
			{
				if (tasktimes_lowprio[i] == 0) continue;
				nexttasktime_lowprio = nexttime_lowprio[i];
				nexttask_lowprio = i;
			}
		}

		//eigentlicher Scheduler
		while (true)
		{
			while (true)
			{
				while (true)
				{
					actualize_nowtime();
					if (!check_highprio()) break;
				}
				if (!check_messages()) break;
			}
			check_lowprio();
		}
	}


#endif
