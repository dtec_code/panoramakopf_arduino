//------------------------------------------------------------
// Description:
//  Tasks zum Auslesen der Eingabe per Touchscreen
//
// Author:
//  Christoph Ilg
//
// Date:
//  24.03.2014
//------------------------------------------------------------


#ifndef GUITASK_CPP
#define GUITASK_CPP

#include "GUItask.h"


unsigned char GUItask::mymenu = 0;
bool GUItask::initedmenu = false;

void GUItask::initmenu()
{
    switch(mymenu)
    {
        case 0:
            STDwindow::settext(0,(unsigned char*)"Test1 Schnittstelle");
            STDwindow::settext(1,(unsigned char*)"");
            STDwindow::settext(2,(unsigned char*)"");
            STDwindow::settext(3,(unsigned char*)"");
            STDwindow::settext(4,(unsigned char*)"");
            STDwindow::setcol(cWHITE, cGRAY, cBLUE, cBLACK, cRED);
            break;
        case 1:
            STDwindow::settext(0,(unsigned char*)"Zylinderpanorama");
            STDwindow::settext(1,(unsigned char*)"Kugelpanorama");
            STDwindow::settext(2,(unsigned char*)"STOP");
            STDwindow::settext(3,(unsigned char*)"");
            STDwindow::settext(4,(unsigned char*)"HOME");
            STDwindow::setcol(cWHITE, cGRAY, cBLUE, cBLACK, cRED);
            break;
        case 2:
            STDwindow::settext(0,(unsigned char*)"");
            STDwindow::settext(1,(unsigned char*)"");
            STDwindow::settext(2,(unsigned char*)"");
            STDwindow::settext(3,(unsigned char*)"");
            STDwindow::settext(4,(unsigned char*)"HOME");
            STDwindow::setcol(cWHITE, cGRAY, cBLUE, cBLACK, cRED);
            break;
    }
}


void GUItask::doaction(unsigned char buttonpressed)
{
    unsigned char *memptr;
    short memaddr;

    switch(mymenu)
    {
        case 0:
            switch(buttonpressed)
            {
                case 0:
                    initedmenu = false;
                    mymenu = 1;
                    break;
                case 1:
                   // initedmenu = false;
                   // mymenu = 2;
                    break;
                case 2:
                    break;
                case 3:
                    break;
                case 4:
                    break;
            }
            break;
        case 1:
            switch(buttonpressed)
            {
                case 0:
                    //sende daten f�r zylinderpanorama an Panoramakopf
                    //fahre 5 Schritte in Schleife mit Pausen dazwischen
                    memaddr = mem::newadr(10);
                    memptr = mem::getptr(memaddr);
                    memptr[0] = 3; //Daten
                    memptr[1] = 236;
                    memptr[2] = 5;

                    memptr[3] = 239;
                    memptr[4] = 20;
                    memptr[5] = 0;
                    memptr[6] = 20;
                    memptr[7] = 241;

                    memptr[8] = 240;
                    memptr[9] = 20;
                    memptr[10] = 0;
                    memptr[11] = 20;
                    memptr[12] = 241;

                    //memptr[9] = 239,
                    //memptr[10] = 20;
                    /*memptr[11] = 0;
                    #memptr[12] = 20;

                    #memptr[13] = 236;
                    #memptr[14] = 20;
                    memptr[15] = 240;
                    memptr[16] = 20;
                    memptr[17] = 0;
                    memptr[18] = 20;
                    memptr[19] = 241;
                    memptr[20] = 20;
                    memptr[21] = 240;
                    memptr[22] = 20;
                    memptr[23] = 0;
                    memptr[24] = 241;
                    memptr[25] = 236;*/
           
                    


                    messager::put(memaddr, CHANNELFORBT_TOSEND);
                    //sende startbefehl an Panoramakopf
                    memaddr = mem::newadr(2);
                    memptr = mem::getptr(memaddr);
                    memptr[0] = 4; //Kommando
                    memptr[1] = 1; //starte
                    messager::put(memaddr, CHANNELFORBT_TOSEND);
                    break;
                case 1:
                    //sende daten f�r kugelpanorama an Panoramakopf
                    //finde Nullposition
                    //fahre 2 hoizontale mal mit Pausen dazwischen
                    //fahre 2 vertikale mal mit Pausen dazwischen
                    memaddr = mem::newadr(34);
                    memptr = mem::getptr(memaddr);
                    memptr[0] = 3; //Daten
                    memptr[1] = 236;

                    memptr[2] = 239;
                    memptr[3] = 40;
                    memptr[4] = 0;

                    memptr[5] = 241;

                    memptr[6] = 239;
                    memptr[7] = 40;
                    memptr[8] = 0;
                    memptr[9] = 241;

                    memptr[10] = 237;
                    memptr[11] = 40;
                    memptr[12] = 0;
                    memptr[13] = 241;
                    //messager::put(memaddr, CHANNELFORBT_TOSEND);

                    //memaddr = mem::newadr(29);
                    //memptr = mem::getptr(memaddr);

                    memptr[14] = 237;
                    memptr[15] = 40;
                    memptr[16] = 0;
                    memptr[17] = 241;

                    memptr[18] = 237;
                    memptr[19] = 40;
                    memptr[20] = 0;
                    memptr[21] = 241;

                    memptr[22] = 237;
                    memptr[23] = 40;
                    memptr[24] = 0;
                    memptr[25] = 241;

                    memptr[26] = 236;

                    memptr[27] = 240;
                    memptr[88] = 40;
                    memptr[29] = 0;


                    memptr[30] = 241;


                    memptr[31] = 240;
                    memptr[32] = 40;
                    memptr[33] = 0;
                    messager::put(memaddr, CHANNELFORBT_TOSEND);

                    //sende startbefehl an Panoramakopf
                    memaddr = mem::newadr(2);
                    memptr = mem::getptr(memaddr);
                    memptr[0] = 4; //Kommando
                    memptr[1] = 1; //starte
                    messager::put(memaddr, CHANNELFORBT_TOSEND);
                    break;
                case 2:
                    //sende stopbefehl an Panoramakopf
                    memaddr = mem::newadr(2);
                    memptr = mem::getptr(memaddr);
                    memptr[0] = 4; //Kommando
                    memptr[1] = 0; //stop
                    messager::put(memaddr, CHANNELFORBT_TOSEND);
                    break;
                case 3:
                    break;
                case 4:
                    initedmenu = false;
                    mymenu = 0;
                    break;
            }
            break;
        case 2:
            switch(buttonpressed)
            {
                case 0:
                    break;
                case 1:
                    break;
                case 2:
                    break;
                case 3:
                    break;
                case 4:
                    initedmenu = false;
                    mymenu = 0;
                    break;
            }
            break;
    }
}


void GUItask::run()
{
    if(!initedmenu)
    {
        initedmenu = true;
        initmenu();
        STDwindow::drawrow(0, false);
        STDwindow::drawrow(1, false);
        STDwindow::drawrow(2, false);
        STDwindow::drawrow(3, false);
        STDwindow::drawrow(4, false);
    }

/*STDwindow::setcol(cWHITE, cGRAY, cBLUE, cBLACK, cRED);
        STDwindow::shownumber(0, false);
        STDwindow::shownumber(1, false);
        STDwindow::shownumber(2, false);
        STDwindow::shownumber(3, false);
        STDwindow::shownumber(4, false);
        STDwindow::shownumber(5, false);
        STDwindow::shownumber(6, false);
        STDwindow::shownumber(7, false);
        STDwindow::shownumber(8, false);
        STDwindow::shownumber(9, false);
        STDwindow::shownumber(10, false);*/
    //Statusleiste
    static bool statuschanged = true;
    static unsigned char BTconnected = 0;
    if (BTtask::state() != BTconnected)
    {
        BTconnected = BTtask::state();
        statuschanged = true;
    }
    if(statuschanged)
    {
        STDwindow::drawstatusbar();
        unsigned short col2 = cBLACK;
        unsigned short col1 = cGRAY;
        if (BTconnected == 4)
        {
            col1 = cWHITE;
            col2 = cBLUE;
        }
        statuschanged = false;
        lcdDrawPicture_compressed(278, 10, col1, col2, col2, 19, 28, logo_BT);
    }
    //--------------
    static char pressed = -1;
    short x;
    short y;
    touchGetXY(&x,&y);
    if ((x==-1)||(y==-1))
    {
        if (pressed != -1)
            STDwindow::drawrow(pressed, false);
        pressed = -1;
        return;
    }
    x *= 2;
    x /= 5;
    y *= 2;
    y /= 7;

    if (x > 240)
    {
        if (pressed != -1)
            STDwindow::drawrow(pressed, false);
        pressed = -1;
        return;
    }
    short oval = y/48;
    if ((oval > 4)||(oval < 0))
    {
        if (pressed != -1)
            STDwindow::drawrow(pressed, false);
        pressed = -1;
        return;
    }
    if (oval!=pressed)
    {
        if (pressed!=-1) STDwindow::drawrow(pressed, false);
        pressed = oval;
        STDwindow::drawrow(oval, true);
        //-----------------Aktionen
        doaction(oval);
    }
}


#endif
