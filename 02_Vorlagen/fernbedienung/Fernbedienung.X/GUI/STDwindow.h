//------------------------------------------------------------
// Description:
//  Das Standardfenster
//
// Author:
//  Christoph Ilg
//
// Date:
//  24.03.2014
//------------------------------------------------------------


#ifndef STDWINDOW_H
#define STDWINDOW_H

#include <plib.h>
#include "../Display/draw_interface.h"

#define TEXTLENGTH 18

class STDwindow
{
private:
    //Die hintergundfarbe
    static unsigned short col_back;
    //Die Hintergrundfarbe der Statusleiste
    static unsigned short col_statusbar;
    //Die Farbe der Buttons
    static unsigned short col_button;
    //Die Farbe eines Button-Schattens
    static unsigned short col_button_shadow;
    //Die Schriftfarbe
    static unsigned short col_font;
    //Der Nachrichteninhalt
    static unsigned char mytext[5][TEXTLENGTH+1];
    //L�nge, die die Texte effektiv haben
    static unsigned char textstartpos[5];
public:
    //initialisiere
 //setze werte auf 0
    //Setze die Farben f�r das Interface
    static void setcol(unsigned short col_back_in,
                       unsigned short col_statusbar_in,
                       unsigned short col_button_in,
                       unsigned short col_button_shadow_in,
                       unsigned short col_font_in);
    //setzt den Text f�r die Zeile
    static void settext(unsigned char row, unsigned char *text);
    //zeichnet die entsprechende button-reihe,
    static void drawrow(unsigned char row, bool pressed);
    //zeichnet die Statusleiste
    static void drawstatusbar();
    //zeigt ziffer auf tastatur an - shadow ist aktiv-farbe
    static void shownumber(unsigned char ziffer, bool pressed);

};


#endif
