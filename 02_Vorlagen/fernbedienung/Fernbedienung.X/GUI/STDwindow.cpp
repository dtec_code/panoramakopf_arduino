//------------------------------------------------------------
// Description:
//  Das Standardfenster
//
// Author:
//  Christoph Ilg
//
// Date:
//  24.03.2014
//------------------------------------------------------------


#ifndef STDWINDOW_CPP
#define STDWINDOW_CPP

#include <plib.h>
#include "STDwindow.h"

    unsigned short STDwindow::col_back;
    unsigned short STDwindow::col_button;
    unsigned short STDwindow::col_button_shadow;
    unsigned short STDwindow::col_font;
    unsigned short STDwindow::col_statusbar;
    unsigned char STDwindow::mytext[5][TEXTLENGTH+1];
    unsigned char STDwindow::textstartpos[5];

    void STDwindow::setcol(unsigned short col_back_in,
                           unsigned short col_statusbar_in,
                           unsigned short col_button_in,
                           unsigned short col_button_shadow_in,
                           unsigned short col_font_in)
    {
        col_back = col_back_in;
        col_statusbar = col_statusbar_in;
        col_button = col_button_in;
        col_button_shadow = col_button_shadow_in;
        col_font = col_font_in;
    }

    void STDwindow::settext(unsigned char row, unsigned char *intext)
    {
        short i = 0;
        while (*(intext+i) != '\0')
        {
            mytext[row][i] = *(intext+i);
            i++;
            if (i==TEXTLENGTH) break;
        }
        mytext[row][i] = '\0';
        textstartpos[row] = i;
    }

    void STDwindow::drawrow(unsigned char row, bool pressed)
    {
        if (row > 4) return;
        //zeichne Hintergrund
        lcdDrawRectangle(48*row, 0, 256, 48, col_back, 0);
        if (textstartpos[row]!=0)
        {
            //zeichen Buttonshadow
            if (!pressed) lcdDrawRectangle(48*row+10, 10, 240, 32, col_button_shadow, 0);
            //zeichne Button
            if (pressed) lcdDrawRectangle(48*row+8, 8, 240, 32, col_button, 0);
            else lcdDrawRectangle(48*row+6, 6, 240, 32, col_button, 0);
            //zeichne Schrift
            if (pressed) lcdWriteString(126-6*textstartpos[row], 48*row+16, mytext[row], &FONT18PX, col_font);
            else lcdWriteString(124-6*textstartpos[row], 48*row+14, mytext[row], &FONT18PX, col_font);
        }
    }

    void STDwindow::drawstatusbar()
    {
        lcdDrawRectangle(0, 256, 64, 240, col_statusbar, 0);
    }


    void STDwindow::shownumber(unsigned char ziffer, bool pressed)
    {
        //ziffer 10 -> ok
        if (ziffer == 10)
        {
            if (pressed)
            {
                lcdDrawRectangle(56,200,48,112,col_button,0);
                lcdWriteChar(213,104, '0', &FONT18PX, col_font);
                lcdWriteChar(225,104, 'K', &FONT18PX, col_font);
            }
            else
            {
                lcdDrawRectangle(58,202,48,112,col_button_shadow,0);
                lcdDrawRectangle(54,198,48,112,col_button,0);
                lcdWriteChar(211,102, '0', &FONT18PX, col_font);
                lcdWriteChar(223,102, 'K', &FONT18PX, col_font);
            }
            return;
        }
        if (ziffer == 0)
        {
            if (pressed)
            {
                lcdDrawRectangle(184,200,48,48,col_button,0);
                lcdWriteChar(220,199, '0', &FONT18PX, col_font);
            }
            else
            {
                lcdDrawRectangle(186,202,48,48,col_button_shadow,0);
                lcdDrawRectangle(182,198,48,48,col_button,0);
                lcdWriteChar(218,197, '0', &FONT18PX, col_font);
            }
        }
        else
        {
            ziffer--;
            unsigned char leftctr = ziffer%3;
            unsigned char downctr = ziffer/3;
            ziffer++;
            if (pressed)
            {
                lcdDrawRectangle(56+64*downctr,8+64*leftctr,48,48,col_button,0);
                lcdWriteChar( 28+64*leftctr,71+64*downctr, ziffer + '0', &FONT18PX, col_font);
            }
            else
            {
                lcdDrawRectangle(58+64*downctr,10+64*leftctr,48,48,col_button_shadow,0);
                lcdDrawRectangle(54+64*downctr,6+64*leftctr,48,48,col_button,0);
                lcdWriteChar( 26+64*leftctr,69+64*downctr, ziffer + '0', &FONT18PX, col_font);
            }
        }
    }


#endif
