//------------------------------------------------------------
// Description:
//  Tasks zum Auslesen der Eingabe per Touchscreen
//
// Author:
//  Christoph Ilg
//
// Date:
//  24.03.2014
//------------------------------------------------------------


#ifndef GUITASK_H
#define GUITASK_H

#include <plib.h>
#include "../Display/touch_driver.h"
#include "../GUI/STDwindow.h"
#include "../Display/draw_interface.h"
#include "../Display/logos.h"
#include "../IlgOS/messager.h"
#include "../IlgOS/mem.h"
#include "../IlgOS/channeldefs.h"
#include "../Bluetooth/BTtask.h"

class GUItask
{
private:
    //das aktuelle Men�
    static unsigned char mymenu;
    //ist das Men� aufgebaut?
    static bool initedmenu;

   // static const unsigned char mydat[1000];
    //f�hre Aktion aus (am besten nur ganz kurz!
    
public:
    //initialisert
    static void init();
    //f�hre Task aus
    static void run();
    //f�hrt Aktion aus
    static void doaction(unsigned char buttonpressed);
    //initialisiert men�
    static void initmenu();
};

#endif
