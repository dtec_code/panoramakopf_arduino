//------------------------------------------------------------
// Description:
//  Tasks zur steuerung der LED
//
// Author:
//  Christoph Ilg
//
// Date:
//  24.03.2014
//------------------------------------------------------------


#ifndef LEDTOGGLETASK_H
#define LEDTOGGLETASK_H

#include <plib.h>

void LEDtoggletask();

#endif
