//------------------------------------------------------------
// Description:
//  Tasks zur steuerung der LED
//
// Author:
//  Christoph Ilg
//
// Date:
//  24.03.2014
//------------------------------------------------------------


#ifndef LEDTOGGLETASK_CPP
#define LEDTOGGLETASK_CPP

#include "LEDtoggletask.h"

void LEDtoggletask()
{
   mPORTGToggleBits(BIT_14);
};

#endif
