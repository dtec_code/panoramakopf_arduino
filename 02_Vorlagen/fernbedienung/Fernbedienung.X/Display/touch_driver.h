//------------------------------------------------------------
// Description:
//  Treiber f�r die Postionsbestimmung bei Ber�hrung
//
//------------------------------------------------------------


#ifndef TOUCH_DRIVER_H
#define TOUCH_DRIVER_H

extern "C"
{

#include <p32xxxx.h>
#include <plib.h>

#define  SAMPLE_PERIOD      100		// us
#define  PRESS_THRESHOULD   256


#define ADC_XPOS        0xD0000
#define ADC_YPOS        0xC0000

#define TOUCH_ORIENTATION 1

// Y port definitions
#define ADPCFG_XPOS     AD1PCFGbits.PCFG13
#define LAT_XPOS        LATBbits.LATB13 
#define LAT_XNEG        LATBbits.LATB11
#define TRIS_XPOS       TRISBbits.TRISB13
#define TRIS_XNEG       TRISBbits.TRISB11 

// X port definitions
#define ADPCFG_YPOS     AD1PCFGbits.PCFG12
#define LAT_YPOS        LATBbits.LATB12  
#define LAT_YNEG        LATBbits.LATB10 
#define TRIS_YPOS       TRISBbits.TRISB12
#define TRIS_YNEG       TRISBbits.TRISB10 


typedef struct touchArea touchArea;
// >> Ringbuffer-Element-Struktur
struct touchArea{
	short top;
	short bottom;
	short left;
	short right;
	
	void (*function)(void);
};


//#define DISP_ORIENTATION 1
#define USE_TOUCH 1

// Globale Variablen
extern short adcX;
extern short adcY;

// Kalibrierungsvariablen
extern short XCalMin;
extern short XCalMax;
extern short YCalMin;
extern short YCalMax;

void touchInit(void);

void touchGetXY(short* x, short* y);
void touchMeasureXY(short* x, short* y);

void touchSetCalData(short xmin, short xmax, short ymin, short ymax);

void touchSetXMin(short xmin);
void touchSetXMax(short xmax);
void touchSetYMin(short ymin);
void touchSetYMax(short ymax);

short touchGetXMin(void);
short touchGetXMax(void);
short touchGetYMin(void);
short touchGetYMax(void);

void initQWERT_SHIFT(void);

}

#endif