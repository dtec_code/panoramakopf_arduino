//------------------------------------------------------------
// Description:
//  Dient zum zeichnen auf das Display
//
//------------------------------------------------------------

#ifndef DRAW_INTERFACE_H
#define DRAW_INTERFACE_H

extern "C"
{

#include <p32xxxx.h>
#include <plib.h>

#include "touch_driver.h"
#include "HX8347D_Driver.h"

//#define DISP_ORIENTATION 0

typedef struct font font;

struct font{
	unsigned char width;
	unsigned char height;
	const unsigned short *characters;
};

extern font FONT12PX;
extern font FONT18PX;

extern const unsigned short FONT_12PX[][12];
extern const unsigned short FONT_18PX[][18];

extern const unsigned short System[76800];

//###########################################################################################################
//
//											Makro-Definitionen
//
//###########################################################################################################
#define PMPWaitBusy()	while(PMMODEbits.BUSY);
//#define DISP_ORIENTATION 0

//###########################################################################################################
//
//											Farb-Definitionen
//
//###########################################################################################################
#define	cBLACK		0x0000
#define cGRAY           0x5555
#define	cWHITE		0xFFFF

#define	cRED		0xF800
#define	cGREEN		0x07E0
#define cBLUE		0x00F8

#define	cYELLOW		0xFFE0
#define	cMAGENTA	0xF01F
#define	cCYAN		0x07FF



typedef unsigned char BYTE;

//###########################################################################################################
//
//											Display-Routinen	
//
//###########################################################################################################

//male einen Button, der gedr�ckt oder nicht gedr�ckt sein kann
//void

void lcdDrawRectangle(unsigned short left, unsigned short top, unsigned short width, unsigned short height, unsigned short color, unsigned char transparency);

void lcdWriteChar(unsigned short col, unsigned short row, unsigned char letter, font * font, unsigned short color);
void lcdWriteString(unsigned short col, unsigned short row, unsigned char * string, font * font, unsigned short color);
void lcdDrawPicture(unsigned short left, unsigned short top, unsigned short width, unsigned short height, const unsigned short * pic);

//von mir: zeichne 2 Bit je Pixel byte
void lcdDrawPicture_compressed(unsigned short left, unsigned short top,
                      unsigned short col1, unsigned short col2,
                      unsigned short col3,
                      unsigned char width, unsigned char height,
                      const unsigned char * pic);

unsigned short lcdGetGreyValue(unsigned short color);
}

#endif