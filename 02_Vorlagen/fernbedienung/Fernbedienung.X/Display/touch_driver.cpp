//------------------------------------------------------------
// Description:
//  Treiber f�r die Postionsbestimmung bei Ber�hrung
//
//------------------------------------------------------------


#ifndef TOUCH_DRIVER_C
#define TOUCH_DRIVER_C

extern "C"
{

#include "touch_driver.h"
#include <plib.h>

short adcX = -1;
short adcY = -1;
short adcPot = 0;

// Bezogen auf DispOrientation 1
short XCalMin = 900;
short XCalMax = 73;
short YCalMin = 80;
short YCalMax = 920;

typedef enum{
SET_X,
RUN_X,
GET_X,
RUN_CHECK_X,
CHECK_X,
SET_Y,
RUN_Y,
GET_Y,
CHECK_Y,
SET_VALUES
} TOUCH_STATES;

TOUCH_STATES state = SET_X;


void touchInit(void) {

	#define TIME_BASE 100

	//INTEnableSystemMultiVectoredInt();

    // Initialize ADC
	AD1CON1 = 0x080E0;  			// Turn on, auto-convert
	AD1CON2 = 0;					// AVdd, AVss, int every conversion, MUXA only
	AD1CON3 = 0x1F80;			    // 31 Tad auto-sample, Tad = 5*Tcy
    AD1CHS = ADC_XPOS;
	AD1PCFG = 0;                    // All inputs are analog
	AD1CSSL = 0;					// No scanned inputs
  
    // Initialize Timer3
    TMR3 = 0;
    PR3 = TIME_BASE;
    T3CONbits.TCKPS = 1;         // Set prescale to 1:8
	IPC3bits.T3IP = 1;
    IFS0bits.T3IF = 0;           // Clear flag
    IEC0bits.T3IE = 1;           // Enable interrupt
    T3CONbits.TON = 1;           // Run timer  
}


void touchSetCalData(short xmin, short xmax, short ymin, short ymax) {
	XCalMin = xmin;
	XCalMax = xmax;
	YCalMin = ymin;
	YCalMax = ymax;
}


void touchGetXY(short* x, short* y) {

	if ( (adcX != -1) && (adcY != -1) ) {
	
		#if (TOUCH_ORIENTATION == 0)
			*x = XCalMin - adcX;
			*y = adcY - YCalMin;	
		#elif (TOUCH_ORIENTATION == 1)
			*x = adcX - XCalMax;
			*y = YCalMax - adcY;
		#endif

		// L�sche Koordinaten um Doppelverwendung zu verhindern
		adcX = -1;
		adcY = -1;
	}
	else {
		*x = -1;
		*y = -1;
	}
}


void touchMeasureXY(short* x, short* y) {

	// L�sche letzte Messung
	adcX = 0;
	adcY = 0;

	while((adcX == -1) && (adcY == -1) );

	*x = adcX;
	*y = adcY;	
}


void touchSetXMin(short xmin) {XCalMin = xmin;}
void touchSetXMax(short xmax) {XCalMax = xmax;}
void touchSetYMin(short ymin) {YCalMin = ymin;}
void touchSetYMax(short ymax) {YCalMax = ymax;}

short touchGetXMin(void) {return XCalMin;}
short touchGetXMax(void) {return XCalMax;}
short touchGetYMin(void) {return YCalMin;}
short touchGetYMax(void) {return YCalMax;}




void __ISR(_TIMER_3_VECTOR, ipl1) _T3Interrupt(void){
	
	static short tempX, tempY;
	short temp;

    switch(state){

        case SET_VALUES:
            if(!AD1CON1bits.DONE)
                break;

            if( (WORD)PRESS_THRESHOULD < (WORD)ADC1BUF0 ){
                adcX = -1; adcY = -1;
            }else{
                adcX = tempX; adcY = tempY;
            }
            state = SET_X;

        case SET_X:
            AD1CHS = ADC_XPOS;     // switch ADC channel

            ADPCFG_XPOS = 0;       // analog
            ADPCFG_YPOS = 1;       // digital

            TRIS_XPOS = 1;
            TRIS_YPOS = 1;
            TRIS_XNEG = 1;
            LAT_YNEG = 0;
            TRIS_YNEG = 0;

            AD1CON1bits.SAMP = 1;  // run conversion
            state = CHECK_X;
            break;

        case CHECK_X:
            if(!AD1CON1bits.DONE)
                break;

            if( (WORD)PRESS_THRESHOULD > (WORD)ADC1BUF0 ){
                LAT_YPOS  = 1;
                TRIS_YPOS = 0;
                tempX = -1;
                state = RUN_X;
            }else{
                adcX = -1; adcY = -1;
                state = SET_X;
                break;
            }

        case RUN_X:
            AD1CON1bits.SAMP = 1;
            state = GET_X;
            break;

        case GET_X:
            if(!AD1CON1bits.DONE)
                break;

            temp = ADC1BUF0;
            if(temp != tempX){
                tempX = temp;
                state = RUN_X;
                break;                                
            }
            TRIS_YPOS = 1;
            AD1CON1bits.SAMP = 1;
            state = SET_Y;
            break;

        case SET_Y:
            if(!AD1CON1bits.DONE)
                break;

            if( (WORD)PRESS_THRESHOULD < (WORD)ADC1BUF0 ){
                adcX = -1; adcY = -1;
                state = SET_X;
                break;
            }

            AD1CHS = ADC_YPOS;     // switch ADC channel

            ADPCFG_XPOS = 1;       // digital
            ADPCFG_YPOS = 0;       // analog

            TRIS_XPOS = 1;
            TRIS_YPOS = 1;
            LAT_XNEG = 0;
            TRIS_XNEG = 0;
            TRIS_YNEG = 1;

            AD1CON1bits.SAMP = 1;  // run conversion
            state = CHECK_Y;
            break;

        case CHECK_Y:
            if(!AD1CON1bits.DONE)
                break;

            if( (WORD)PRESS_THRESHOULD > (WORD)ADC1BUF0 ){
                LAT_XPOS  = 1;
                TRIS_XPOS = 0;
                tempY = -1;
                state = RUN_Y;
            }else{
                adcX = -1; adcY = -1;
                state = SET_X;
                break;
            }

        case RUN_Y:
            AD1CON1bits.SAMP = 1;
            state = GET_Y;
            break;

        case GET_Y:
            // Get Y value
            if(!AD1CON1bits.DONE)
                break;

            temp = ADC1BUF0;
            if(temp != tempY){
                tempY = temp;
                state = RUN_Y;
                break;                                
            }
            TRIS_XPOS = 1;
            AD1CON1bits.SAMP = 1;
            state = SET_VALUES;
            break;

        default:
            state = SET_X;
    }

    // Clear flag
    IFS0bits.T3IF = 0;
}

}

#endif