//------------------------------------------------------------
// Description:
//  Treiber f�r die Grafikausgabe
//
//------------------------------------------------------------


#ifndef HX8347D_DRIVER_H
#define HX8347D_DRIVER_H

extern "C"
{

#include <p32xxxx.h>
#include <plib.h>

#include "touch_driver.h"

#define DISP_ORIENTATION 0


//###########################################################################################################
//
//											Makro-Definitionen
//
//###########################################################################################################
#define PMPWaitBusy()	while(PMMODEbits.BUSY);
//#define DISP_ORIENTATION 0



typedef unsigned char BYTE;

//###########################################################################################################
//
//											Display-Routinen	
//
//###########################################################################################################
// LCD RST (Reset) Pin
#define _LCD_RST_T		_TRISC1
#define _LCD_RST		_LATC1

// LCD RS (Register Select) Pin
#define _LCD_RS_T		_TRISB15
#define _LCD_RS			_LATB15

// LCD CS (Chip Selection) Pin
#define _LCD_CS_T		_TRISF12
#define _LCD_CS			_LATF12



#define SetIndex(index)		\
	_LCD_RS = 0;        	\
	PMDIN = index;     	\
	PMPWaitBusy();


#define WriteCommand(cmd) 	\
	_LCD_RS = 1;           	\
	PMDIN = cmd;         	\
	PMPWaitBusy();

#define WriteData(data) 	\
    _LCD_RS = 1;         	\
    PMDIN = data;          \
    PMPWaitBusy();


#define SetAddress(x, y)                  		\
    SetIndex(0x02);                           	\
    WriteCommand((x>>8)&0x00FF); 	\
    SetIndex(0x03);                           	\
    WriteCommand(x&0x00FF); 	\
    SetIndex(0x06);                           	\
    WriteCommand((y>>8)&0x00FF); 	\
    SetIndex(0x07);                           	\
    WriteCommand(y&0x00FF); 	\
    SetIndex(0x22);



/*****************************************************
	Anschlussbelegung:	Backlight
*****************************************************/
#define LCD_BKL_Periode	1000	// 10ms Periode

#define _LCD_BKL_T		TRISAbits.TRISA9
#define _LCD_BKL		PORTAbits.RA9

#define LCD_TMR_IF	IFS0bits.T2IF		// TMR2-Interrupt-Flag
#define LCD_TMR_IE	IEC0bits.T2IE		// TMR2-Interrupt-Enable
#define LCD_TMR_IP	IPC2bits.T2IP		// TMR2-Interrupt-Priority			
#define LCD_TMR_ON	T2CONbits.ON		// TMR2 On
#define LCD_TMR_Reg	PR2					// TMR2 Count-Register



extern int bklDC;
extern int bklTimeHigh;
extern int bklTimeLow;
extern char bklStateHigh;

int initLCD(void);



void lcdSetReg(BYTE index, BYTE value);
void lcdSetActiveWinArea(unsigned short left, unsigned short top, unsigned short width, unsigned short height);
unsigned short lcdGetPixel(unsigned short x, unsigned short y);
unsigned short ReadData(void);
void lcdSetPixel(unsigned short x, unsigned short y, unsigned short color);
int lcdSetPixelColor(unsigned short color);
unsigned short lcdSetPixelAddress(unsigned short x, unsigned short y);

void lcdDelay(int time_ms);

int lcdSetBKL(int dutyCycle);
void lcdBKLOn(void);
void lcdBKLOff(void);

unsigned short lcdGetGreyValue(unsigned short color);

//extern void __ISR(_TIMER_2_VECTOR, ipl4) _LCD_BKL_TMR2_ISR(void);

}

#endif