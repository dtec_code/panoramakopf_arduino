//------------------------------------------------------------
// Description:
//  Treiber f�r die Grafikausgabe
//
//------------------------------------------------------------

#ifndef HX8347D_DRIVER_C
#define HX8347D_DRIVER_C


#include "HX8347D_Driver.h"

extern "C"
{

int bklDC = 0;
int bklTimeHigh = 500;
int bklTimeLow = 500;
char bklStateHigh = 1;

char clearDelay = 0;
int lcdDelayCnt = 0;

extern const unsigned char font[1][12];


//###########################################################################################################
//
//                                          Display-Routinen
//
//###########################################################################################################


//###########################################################################################################
//
//	Funktion:			void initLCD(void)
//
//	Beschreibung:			Initialisierung des LCDs, Hintergrundbeleuchtung und Touchscreen.
//
//	�bergabeparameter:		-
//	R�ckgabeparameter:		-
//
//###########################################################################################################

int initLCD(void) {
	// Aktiviere Multi-Vectored-Interrupt
	//INTEnableSystemMultiVectoredInt();

	/********************************************************************************************************
            Initialisiere Backlight-Funktionalit�t
	********************************************************************************************************/
	
	// Initialisiere Backlight (Timer 2)
	//lcdSetBKL(50);		// BKL-DT
	_LCD_BKL_T = 0;		// BKL-Pin: Ausgang
	//lcdBKLOff();			// Initialisiere ersten High-Puls
	
	// Timer f�r Backlight-PWM	(TMR2)
	T2CON = 0x0000;
	PR2 = 1000;
	LCD_TMR_IP = 2;		// Setze TMR2 Interrupt-Priorit�t auf 1
	LCD_TMR_IE = 1;		// Enable TMR2 Interrupt
	LCD_TMR_IF = 0;		// L�sche TMR2 RX-Interrupt-Flag
	LCD_TMR_ON = 1;		// Aktiviere TMR2


	/********************************************************************************************************
            Initialisiere PMP-Modul
	********************************************************************************************************/
	_LCD_RST = 0;		// F�r Config in Reset halten
	_LCD_RST_T = 0;

	_LCD_RS = 1;		// Schreibender Zugriff
	_LCD_RS_T = 0;

	_LCD_CS = 1;		// Disable LCD
	_LCD_CS_T = 0;

	

	/********************************************************************************************************
            Initialisiere PMP-Modul
	********************************************************************************************************/
	
	// PMMODE - Register
	PMMODE = 0;					// L�sche PMODE-Register

	// PMAEN - Register
	PMAEN = 0;					// Alle PMA-Pins als digitale I/Os

	// PMCON - Register
	PMCON = 0;					// L�sche PMCON-Register

	PMMODEbits.MODE = 2;		// Master Mode 2
	PMMODEbits.WAITB = 0;		// 1 TPB
	PMMODEbits.WAITM = 3;		// 4 TPB
	PMMODEbits.WAITE = 0;		// 1 TPB
	PMMODEbits.MODE16 = 1;		// Aktiviere 16-Bit Datenbus
	PMCONbits.CSF = 0;
	PMCONbits.PTRDEN = 1;		// Read/Write Strobe Port Enable bit
	PMCONbits.PTWREN = 1;		// Write Enable Strobe Port Enable bit
	PMCONbits.PMPEN = 1;		// Aktiviere PMP-Modul

//	PMMODEbits.INCM = 0;		// Kein Inkr. (Adress-Port nicht verwendet)
//	PMMODEbits.IRQM = 0;		// Interrupt derzeit nicht verwendet

	lcdDelay(100);
	_LCD_RST = 1;
	lcdDelay(150);

    // Driving ability setting
	lcdSetReg(0xEA,0x00);   // PTBA[15:8]
	lcdSetReg(0xEB,0x20);   // PTBA[7:0]
	lcdSetReg(0xEC,0x0C);   // STBA[15:8]
	lcdSetReg(0xED,0xC4);   // STBA[7:0]
	lcdSetReg(0xE8,0x40);   // OPON[7:0]
	lcdSetReg(0xE9,0x38);   // OPON1[7:0]
	lcdSetReg(0xF1,0x01);   // OTPS1B
	lcdSetReg(0xF2,0x10);   // GEN
	lcdSetReg(0x27,0xA3);   // 

    // Gamma 2.8 setting 
	lcdSetReg(0x40,0x00);   // 
	lcdSetReg(0x41,0x00);   // 
	lcdSetReg(0x42,0x01);   // 
	lcdSetReg(0x43,0x13);   // 
	lcdSetReg(0x44,0x10);   // 
	lcdSetReg(0x45,0x26);   // 
	lcdSetReg(0x46,0x08);   // 
	lcdSetReg(0x47,0x51);   // 
	lcdSetReg(0x48,0x02);   // 
	lcdSetReg(0x49,0x12);   // 
	lcdSetReg(0x4A,0x18);   // 
	lcdSetReg(0x4B,0x19);   // 
	lcdSetReg(0x4C,0x14);   // 

	lcdSetReg(0x50,0x19);   // 
	lcdSetReg(0x51,0x2F);   // 
	lcdSetReg(0x52,0x2C);   // 
	lcdSetReg(0x53,0x3E);   // 
	lcdSetReg(0x54,0x3F);   //          
	lcdSetReg(0x55,0x3F);   // 
	lcdSetReg(0x56,0x2E);   // 
	lcdSetReg(0x57,0x77);   // 
	lcdSetReg(0x58,0x0B);   // 
	lcdSetReg(0x59,0x06);   //          
	lcdSetReg(0x5A,0x07);   // 
	lcdSetReg(0x5B,0x0D);   // 
	lcdSetReg(0x5C,0x1D);   // 
	lcdSetReg(0x5D,0xCC);   // 


    // Window setting
   	lcdSetReg(0x02,0x00);	// 0
   	lcdSetReg(0x03,0x00);	
   	lcdSetReg(0x04,0x01);	// 319
   	lcdSetReg(0x05,0x3F);	
   	lcdSetReg(0x06,0x00);	// 0
   	lcdSetReg(0x07,0x00);	
	lcdSetReg(0x08,0x00);	// 239
   	lcdSetReg(0x09,0xEF);	


    // Display Setting
    // SetReg(0x01,0x06);   // IDMON=0, INVON=1, NORON=1, PTLON=0


	#if (DISP_ORIENTATION == 0)
		lcdSetReg(0x16,0b10101000);   // MY=0, MX=0, MV=0, BGR=1
	#elif (DISP_ORIENTATION == 1)
		lcdSetReg(0x16,0b01101000);   // MY=1, MX=1, MV=0, BGR=1
	#endif


    // Power Voltage Setting
	lcdSetReg(0x1B,0x1B);   // VRH = 4.65
	lcdSetReg(0x1A,0x01);   // BT
	lcdSetReg(0x24,0x2F);   // VMH
	lcdSetReg(0x25,0x57);   // VML
	
	// Vcom offset
	lcdSetReg(0x23,0x8D);   // FLICKER ADJUST

        // Power ON Setting
	lcdSetReg(0x18,0x36);   // 
	lcdSetReg(0x19,0x01);   //  
	lcdSetReg(0x01,0x00);   // 
	lcdSetReg(0x1F,0x88);   // 	
	lcdDelay(5); 
	lcdSetReg(0x1F,0x80);   // 	
	lcdDelay(5); 
	lcdSetReg(0x1F,0x90);   // 	
	lcdDelay(5); 
	lcdSetReg(0x1F,0xD0);   // 	
	lcdDelay(5); 

    // 65K Color Selection
	lcdSetReg(0x17,0x05);   // 	
    
    // Set Panel
  	lcdSetReg(0x36,0x00);   // 	

    // Display ON Setting
	lcdSetReg(0x28,0x38);   // 	
	lcdDelay( 40); 
	lcdSetReg(0x28,0x3C);   // 
}



//###########################################################################################################
//
//	Funktion:               void lcdSetReg(void)
//
//	Beschreibung:			
//
//	�bergabeparameter:	index (int)	-	Delay-Zeit in ms
//						value (int)	-	Delay-Zeit in ms
//	R�ckgabeparameter:	-
//
//###########################################################################################################
void lcdSetReg(BYTE index, BYTE value) {
	// Chip-Zugriff
	_LCD_CS = 0;

	SetIndex(index);
	WriteCommand(value);

    // Chip-Sperrung
    _LCD_CS = 1;
}



void lcdSetActiveWinArea(unsigned short left, unsigned short top, unsigned short width, unsigned short height) {
	unsigned short bottom, right;

	bottom = top + height - 1;
	right = left + width - 1;

	// Definiere aktive Fenster-Sektion
   	lcdSetReg(0x02,left>>8);
   	lcdSetReg(0x03,left & 0x00FF);	
   	lcdSetReg(0x04,right>>8);
   	lcdSetReg(0x05,right & 0x00FF);	
   	lcdSetReg(0x06,top>>8);
   	lcdSetReg(0x07,top & 0x00FF);
	lcdSetReg(0x08,bottom>>8);
   	lcdSetReg(0x09,bottom & 0x00FF);
}

//###########################################################################################################
//
//	Funktion:		void lcdSetPixel(int x, int y)
//
//	Beschreibung:			
//
//	�bergabeparameter:	x (int)		-	Display-Spalte
//						y (int)		-	Display-Zeile
//	R�ckgabeparameter:	0			-	Adresse/Farbwert g�ltig
//						-1      	-	Adresse ung�ltig
//						-2			-	Farbwert ung�ltig
//
//###########################################################################################################
void lcdSetPixel(unsigned short x, unsigned short y, unsigned short color) {
	if(x>319 || x<0 || y>239 || y<0) exit;

	_LCD_CS=0;

	SetAddress(x,y);
	WriteData(color);

	_LCD_CS=1;
}

unsigned short ReadData(void){
    unsigned short dat;
    
    _LCD_RS = 1;
    dat = PMDIN;          // start a read sequence
    PMPWaitBusy();      // wait for read completion
    dat = PMDIN;
    return (dat);
}

unsigned short lcdGetPixel(unsigned short x, unsigned short y){
    unsigned short val = 0;
	unsigned short temp = 0;
    
	_LCD_CS = 0;
    SetAddress(x,y);

	ReadData();
	ReadData();

	temp = ReadData();
	val = temp & 0xF800;
	val |= (temp<<3) & 0x7E0;  

	temp = ReadData();

	val |= (temp>>3)& 0x001F;   
    
    _LCD_CS = 1;   
    
	return val;
}


unsigned short lcdSetPixelAddress(unsigned short x, unsigned short y) {
	if(x>319 || x<0 || y>239 || y<0) return -1;

	// Setze x-Adresse
	lcdSetReg(0x0002, (x>>8)&0x00FF);
	lcdSetReg(0x0003, x&0x00FF);

	// Setze y-Adresse
	lcdSetReg(0x0006, (y>>8)&0x00FF);
	lcdSetReg(0x0007, y&0x00FF);

	return 0;
}

int lcdSetPixelColor(unsigned short color) {
	
	// Pr�fe, ob Farbe g�ltig!

	lcdSetReg(0x0022, color);
}



//###########################################################################################################
//
//	Funktion:		void lcdDelay(void)
//
//	Beschreibung:		Delay f�r �bergebene Zeit (in ms). Delay kann l�nger dauern, wie �bergeben.
//
//	�bergabeparameter:	time_ms (int)   	-	Delay-Zeit in ms
//	R�ckgabeparameter:	-
//
//###########################################################################################################
void lcdDelay(int time_ms) {
	// Setze aktuellen Delay-Counter in ISR auf 0
	clearDelay = 1;

	// Warte, bis Delay-Counter auf 0 gesetzt
	while(lcdDelayCnt != 0);

	clearDelay = 0;

	// Warte f�r spezifische Delay-Zeit
	while(lcdDelayCnt < time_ms);
}







//###########################################################################################################
//
//                                          Backlight-Routinen
//
//###########################################################################################################


//###########################################################################################################
//
//	Funktion:               void lcdBKLOn(void)
//
//	Beschreibung:		Schaltet Hintergrundbeleuchtung an und initialisiert Timer-Z�hlregister
//				f�r die Zeitspanne TimeHigh (anhand Backlight-Duty-Cycle bestimmt).
//
//	�bergabeparameter:	-
//	R�ckgabeparameter:	-
//
//###########################################################################################################
void lcdBKLOn(void) {
	LCD_TMR_Reg = bklTimeHigh;
	_LCD_BKL = 1;
	bklStateHigh = 1;
}



//###########################################################################################################
//
//	Funktion:		void lcdBKLOff(void)
//
//	Beschreibung:		Schaltet Hintergrundbeleuchtung aus und initialisiert Timer-Z�hlregister
//				f�r die Zeitspanne TimeLow (anhand Backlight-Duty-Cycle bestimmt).
//
//	�bergabeparameter:	-
//	R�ckgabeparameter:	-
//
//###########################################################################################################
void lcdBKLOff(void) {
	LCD_TMR_Reg = bklTimeLow;
	_LCD_BKL = 0;
	bklStateHigh = 0;
}



//###########################################################################################################
//
//	Funktion:		int lcdSetBKL(int dutyCycle)
//
//	Beschreibung:		Berechnet aus �bergebenem Duty-Cycle die neuen Zeiten TimeHigh und Timelow.
//				Die Frequenz der Hintergrundbeleuchtung entspricht 100 Hz.
//
//	�bergabeparameter:	dutyCycle (int)		-	Duty Cycle f�r Backlight (Wertebereich: 0 ... 100)
//	R�ckgabeparameter:	0			-	�bergebener Parameter g�ltig	(innerhalb Wertebereich)
//				-1      		-	�bergebener Parameter ung�ltig 	(au�erhalb Wertebereich)
//
//###########################################################################################################
int lcdSetBKL(int dutyCycle) {
	if(dutyCycle>=0 && dutyCycle<101) {
		bklDC = dutyCycle;
		bklTimeHigh = (LCD_BKL_Periode * dutyCycle)/100;
		bklTimeLow = LCD_BKL_Periode - bklTimeHigh;
	}
	else return -1;

	return 0;
}



//###########################################################################################################
//
//	Funktion:		void _LCD_BKL_TMR2_ISR(void)
//
//	Beschreibung:		ISR f�r Backlight-Timer. Toggelt Hintergrundbeleuchtung und initialisiert
//				entsprechend dem aktuellen Ausgangspegel das Timer-Z�hlregister.
//
//	�bergabeparameter:	-
//	R�ckgabeparameter:	-
//
//###########################################################################################################
void __ISR(_TIMER_2_VECTOR, ipl2) _LCD_BKL_TMR2_ISR(void) {
	//if(bklStateHigh == 1) 		lcdBKLOff();
	//else if(bklStateHigh == 0) 	lcdBKLOn();

	if(clearDelay) lcdDelayCnt = 0;
	//else if(!clearDelay && bklStateHigh) lcdDelayCnt += 10;
	else if(!clearDelay) lcdDelayCnt += 10;

	LCD_TMR_IF = 0;
}

}

#endif