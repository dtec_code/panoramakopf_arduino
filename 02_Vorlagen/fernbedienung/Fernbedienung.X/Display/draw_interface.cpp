//------------------------------------------------------------
// Description:
//  Dient zum zeichnen auf das Display
//
//------------------------------------------------------------

#ifndef DRAW_INTERFACE_C
#define DRAW_INTERFACE_C


#include "draw_interface.h"

extern "C"
{

//###########################################################################################################
//
//                                          Display-Routinen
//
//###########################################################################################################



void lcdDrawRectangle(unsigned short top, unsigned short left, unsigned short width, unsigned short height, unsigned short color, unsigned char transparency) {
	unsigned short col, row, origColor, pixelColor,r,g,b,val,temp;

	if (transparency == 0) {
		//for(row=top; row<(top+height); row++) {
		//	for(col=left; col<(left+width); col++) {
				lcdSetActiveWinArea(left, top, width, height);
				_LCD_CS=0;
				SetAddress(left,top);
			
				for(row=0; row<height; row++) {
					for(col=0; col<width; col++) {
						WriteData(color);
					}
				}
				_LCD_CS=1;
		//	}
	//	}
	}
	else {
		for(row=top; row<(top+height); row++) {
			for(col=left; col<(left+width); col++) {
				origColor = lcdGetPixel(col, row);
				val = lcdGetGreyValue(origColor);

				r = (color>>11) & 0x001F;
				g = (color>>6) & 0x001F;
				b = color & 0x1F;

				temp = r+(val*transparency) /100;
				if (temp<0x001F) r=temp;
				
				temp = g+(val*transparency) /100;
				if (temp<0x003F) g=temp;

				temp = b+(val*transparency) /100;
				if (temp<0x001F) b=temp;

				pixelColor = 0;
				pixelColor = ((r&0x001F) << 11) | ((g&0x003F) << 5) | (b&0x001F);

				lcdSetPixel(col, row, pixelColor);
			}
		}
	}
}


void lcdDrawPicture(unsigned short left, unsigned short top, unsigned short width, unsigned short height, const unsigned short * pic) {
	
	unsigned short col, row;
	int i;

	lcdSetActiveWinArea(left, top, width, height);

	_LCD_CS=0;

	SetAddress(left,top);

	for(i=0; i<width*height; i++) {
		WriteData(pic[i]);
	}

	_LCD_CS=1;
}


unsigned short lcdGetGreyValue(unsigned short color) {
	unsigned short r,g,b,val;
	
	r = (color>>11) & 0x001F;
	g = (color>>6) & 0x001F;
	b = color & 0x1F;

	val = (r+b+g)/3;

	return val;
}

void lcdWriteChar(unsigned short col, unsigned short row, unsigned char letter, font * font, unsigned short color) {
	int i, j;
	int temp;
	short charRow;
	int offset;

		for(j=0; j<font->height; j++) {
		temp = 0x8000;
		offset = (letter-0x20)*(font->height)+j;
		charRow = font->characters[offset];

		for (i=0; i<font->width; i++) {
			if(charRow & temp) {
				lcdSetPixel(i+col, row, color);
			}
			temp = temp>>1;
		}
		row++;
	}
}

void lcdWriteString(unsigned short col, unsigned short row, unsigned char * string, font * font, unsigned short color) {
	int i=0;

	while(string[i]!= '\0') {
		lcdWriteChar(col+i*font->width, row, string[i], font, color);
		i++;
	}
}


void lcdDrawPicture_compressed(unsigned short left, unsigned short top,
                      unsigned short col1, unsigned short col2,
                      unsigned short col3,
                      unsigned char width, unsigned char height,
                      const unsigned char * pic)
{
    unsigned short arrayctr = 1;
    unsigned char myval = *pic;
    unsigned char myctr = 0;
    unsigned char maskval;
    for (unsigned short toppos = top;toppos<(top+height);toppos++)
    {
        for (unsigned short leftpos = left;leftpos<(left+width);leftpos++)
        {
            maskval = ((myval>>myctr)&3);
            switch(maskval)
            {
                case 0:
                    break;
                case 1:
                    lcdSetPixel(leftpos, toppos, col1);
                    break;
                case 2:
                    lcdSetPixel(leftpos, toppos, col2);
                    break;
                case 3:
                    lcdSetPixel(leftpos, toppos, col3);
                    break;
            }
            myctr+=2;
            if (myctr == 8)
            {
                myctr = 0;
                myval = *(pic+arrayctr++);
            }
        }
    }
}


}

#endif