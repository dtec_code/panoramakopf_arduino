/* 
 * File:   Main.C
 * Author: Christoph
 *
 * Created on 23. M�rz 2014, 11:06
 */

#include "startdefines.h"
#include <cstdlib>
#include <GenericTypeDefs.h>
#include <p32xxxx.h>
#include <plib.h>
#include "Display/fonts.h"
#include "Display\HX8347D_driver.h"
#include "Display\draw_interface.h"
#include "Display\touch_driver.h"
#include "GUI/STDwindow.h"
#include "GUI/GUItask.h"
#include "Bluetooth/MYUART.h"

#include "IlgOS\ilgOS.h"


int main(int argc, char** argv) {

    //startet IlgOS
    runilgOS();
    return 0;
};


