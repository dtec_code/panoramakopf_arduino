#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-default.mk)" "nbproject/Makefile-local-default.mk"
include nbproject/Makefile-local-default.mk
endif
endif

# Environment
MKDIR=gnumkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=default
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=elf
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/Fernbedienung.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/Fernbedienung.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=Bluetooth/BTtask.cpp Bluetooth/MYUART.cpp Display/HX8347D_driver.cpp Display/draw_interface.cpp Display/touch_driver.cpp GUI/STDwindow.cpp GUI/GUItask.cpp IlgOS/mem.cpp IlgOS/messager.cpp IlgOS/scheduler.cpp library/stringlib.cpp Tasks/LEDtoggletask.cpp Main.cpp

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/Bluetooth/BTtask.o ${OBJECTDIR}/Bluetooth/MYUART.o ${OBJECTDIR}/Display/HX8347D_driver.o ${OBJECTDIR}/Display/draw_interface.o ${OBJECTDIR}/Display/touch_driver.o ${OBJECTDIR}/GUI/STDwindow.o ${OBJECTDIR}/GUI/GUItask.o ${OBJECTDIR}/IlgOS/mem.o ${OBJECTDIR}/IlgOS/messager.o ${OBJECTDIR}/IlgOS/scheduler.o ${OBJECTDIR}/library/stringlib.o ${OBJECTDIR}/Tasks/LEDtoggletask.o ${OBJECTDIR}/Main.o
POSSIBLE_DEPFILES=${OBJECTDIR}/Bluetooth/BTtask.o.d ${OBJECTDIR}/Bluetooth/MYUART.o.d ${OBJECTDIR}/Display/HX8347D_driver.o.d ${OBJECTDIR}/Display/draw_interface.o.d ${OBJECTDIR}/Display/touch_driver.o.d ${OBJECTDIR}/GUI/STDwindow.o.d ${OBJECTDIR}/GUI/GUItask.o.d ${OBJECTDIR}/IlgOS/mem.o.d ${OBJECTDIR}/IlgOS/messager.o.d ${OBJECTDIR}/IlgOS/scheduler.o.d ${OBJECTDIR}/library/stringlib.o.d ${OBJECTDIR}/Tasks/LEDtoggletask.o.d ${OBJECTDIR}/Main.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/Bluetooth/BTtask.o ${OBJECTDIR}/Bluetooth/MYUART.o ${OBJECTDIR}/Display/HX8347D_driver.o ${OBJECTDIR}/Display/draw_interface.o ${OBJECTDIR}/Display/touch_driver.o ${OBJECTDIR}/GUI/STDwindow.o ${OBJECTDIR}/GUI/GUItask.o ${OBJECTDIR}/IlgOS/mem.o ${OBJECTDIR}/IlgOS/messager.o ${OBJECTDIR}/IlgOS/scheduler.o ${OBJECTDIR}/library/stringlib.o ${OBJECTDIR}/Tasks/LEDtoggletask.o ${OBJECTDIR}/Main.o

# Source Files
SOURCEFILES=Bluetooth/BTtask.cpp Bluetooth/MYUART.cpp Display/HX8347D_driver.cpp Display/draw_interface.cpp Display/touch_driver.cpp GUI/STDwindow.cpp GUI/GUItask.cpp IlgOS/mem.cpp IlgOS/messager.cpp IlgOS/scheduler.cpp library/stringlib.cpp Tasks/LEDtoggletask.cpp Main.cpp


CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-default.mk dist/${CND_CONF}/${IMAGE_TYPE}/Fernbedienung.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=32MX460F512L
MP_LINKER_FILE_OPTION=
# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assembleWithPreprocess
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compileCPP
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/Bluetooth/BTtask.o: Bluetooth/BTtask.cpp  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/Bluetooth 
	@${RM} ${OBJECTDIR}/Bluetooth/BTtask.o.d 
	@${RM} ${OBJECTDIR}/Bluetooth/BTtask.o 
	@${FIXDEPS} "${OBJECTDIR}/Bluetooth/BTtask.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CPPC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c++ -c -mprocessor=$(MP_PROCESSOR_OPTION)  -frtti -fexceptions -fno-check-new -fenforce-eh-specs -MMD -MF "${OBJECTDIR}/Bluetooth/BTtask.o.d" -o ${OBJECTDIR}/Bluetooth/BTtask.o Bluetooth/BTtask.cpp  
	
${OBJECTDIR}/Bluetooth/MYUART.o: Bluetooth/MYUART.cpp  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/Bluetooth 
	@${RM} ${OBJECTDIR}/Bluetooth/MYUART.o.d 
	@${RM} ${OBJECTDIR}/Bluetooth/MYUART.o 
	@${FIXDEPS} "${OBJECTDIR}/Bluetooth/MYUART.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CPPC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c++ -c -mprocessor=$(MP_PROCESSOR_OPTION)  -frtti -fexceptions -fno-check-new -fenforce-eh-specs -MMD -MF "${OBJECTDIR}/Bluetooth/MYUART.o.d" -o ${OBJECTDIR}/Bluetooth/MYUART.o Bluetooth/MYUART.cpp  
	
${OBJECTDIR}/Display/HX8347D_driver.o: Display/HX8347D_driver.cpp  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/Display 
	@${RM} ${OBJECTDIR}/Display/HX8347D_driver.o.d 
	@${RM} ${OBJECTDIR}/Display/HX8347D_driver.o 
	@${FIXDEPS} "${OBJECTDIR}/Display/HX8347D_driver.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CPPC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c++ -c -mprocessor=$(MP_PROCESSOR_OPTION)  -frtti -fexceptions -fno-check-new -fenforce-eh-specs -MMD -MF "${OBJECTDIR}/Display/HX8347D_driver.o.d" -o ${OBJECTDIR}/Display/HX8347D_driver.o Display/HX8347D_driver.cpp  
	
${OBJECTDIR}/Display/draw_interface.o: Display/draw_interface.cpp  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/Display 
	@${RM} ${OBJECTDIR}/Display/draw_interface.o.d 
	@${RM} ${OBJECTDIR}/Display/draw_interface.o 
	@${FIXDEPS} "${OBJECTDIR}/Display/draw_interface.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CPPC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c++ -c -mprocessor=$(MP_PROCESSOR_OPTION)  -frtti -fexceptions -fno-check-new -fenforce-eh-specs -MMD -MF "${OBJECTDIR}/Display/draw_interface.o.d" -o ${OBJECTDIR}/Display/draw_interface.o Display/draw_interface.cpp  
	
${OBJECTDIR}/Display/touch_driver.o: Display/touch_driver.cpp  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/Display 
	@${RM} ${OBJECTDIR}/Display/touch_driver.o.d 
	@${RM} ${OBJECTDIR}/Display/touch_driver.o 
	@${FIXDEPS} "${OBJECTDIR}/Display/touch_driver.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CPPC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c++ -c -mprocessor=$(MP_PROCESSOR_OPTION)  -frtti -fexceptions -fno-check-new -fenforce-eh-specs -MMD -MF "${OBJECTDIR}/Display/touch_driver.o.d" -o ${OBJECTDIR}/Display/touch_driver.o Display/touch_driver.cpp  
	
${OBJECTDIR}/GUI/STDwindow.o: GUI/STDwindow.cpp  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/GUI 
	@${RM} ${OBJECTDIR}/GUI/STDwindow.o.d 
	@${RM} ${OBJECTDIR}/GUI/STDwindow.o 
	@${FIXDEPS} "${OBJECTDIR}/GUI/STDwindow.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CPPC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c++ -c -mprocessor=$(MP_PROCESSOR_OPTION)  -frtti -fexceptions -fno-check-new -fenforce-eh-specs -MMD -MF "${OBJECTDIR}/GUI/STDwindow.o.d" -o ${OBJECTDIR}/GUI/STDwindow.o GUI/STDwindow.cpp  
	
${OBJECTDIR}/GUI/GUItask.o: GUI/GUItask.cpp  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/GUI 
	@${RM} ${OBJECTDIR}/GUI/GUItask.o.d 
	@${RM} ${OBJECTDIR}/GUI/GUItask.o 
	@${FIXDEPS} "${OBJECTDIR}/GUI/GUItask.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CPPC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c++ -c -mprocessor=$(MP_PROCESSOR_OPTION)  -frtti -fexceptions -fno-check-new -fenforce-eh-specs -MMD -MF "${OBJECTDIR}/GUI/GUItask.o.d" -o ${OBJECTDIR}/GUI/GUItask.o GUI/GUItask.cpp  
	
${OBJECTDIR}/IlgOS/mem.o: IlgOS/mem.cpp  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/IlgOS 
	@${RM} ${OBJECTDIR}/IlgOS/mem.o.d 
	@${RM} ${OBJECTDIR}/IlgOS/mem.o 
	@${FIXDEPS} "${OBJECTDIR}/IlgOS/mem.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CPPC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c++ -c -mprocessor=$(MP_PROCESSOR_OPTION)  -frtti -fexceptions -fno-check-new -fenforce-eh-specs -MMD -MF "${OBJECTDIR}/IlgOS/mem.o.d" -o ${OBJECTDIR}/IlgOS/mem.o IlgOS/mem.cpp  
	
${OBJECTDIR}/IlgOS/messager.o: IlgOS/messager.cpp  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/IlgOS 
	@${RM} ${OBJECTDIR}/IlgOS/messager.o.d 
	@${RM} ${OBJECTDIR}/IlgOS/messager.o 
	@${FIXDEPS} "${OBJECTDIR}/IlgOS/messager.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CPPC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c++ -c -mprocessor=$(MP_PROCESSOR_OPTION)  -frtti -fexceptions -fno-check-new -fenforce-eh-specs -MMD -MF "${OBJECTDIR}/IlgOS/messager.o.d" -o ${OBJECTDIR}/IlgOS/messager.o IlgOS/messager.cpp  
	
${OBJECTDIR}/IlgOS/scheduler.o: IlgOS/scheduler.cpp  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/IlgOS 
	@${RM} ${OBJECTDIR}/IlgOS/scheduler.o.d 
	@${RM} ${OBJECTDIR}/IlgOS/scheduler.o 
	@${FIXDEPS} "${OBJECTDIR}/IlgOS/scheduler.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CPPC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c++ -c -mprocessor=$(MP_PROCESSOR_OPTION)  -frtti -fexceptions -fno-check-new -fenforce-eh-specs -MMD -MF "${OBJECTDIR}/IlgOS/scheduler.o.d" -o ${OBJECTDIR}/IlgOS/scheduler.o IlgOS/scheduler.cpp  
	
${OBJECTDIR}/library/stringlib.o: library/stringlib.cpp  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/library 
	@${RM} ${OBJECTDIR}/library/stringlib.o.d 
	@${RM} ${OBJECTDIR}/library/stringlib.o 
	@${FIXDEPS} "${OBJECTDIR}/library/stringlib.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CPPC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c++ -c -mprocessor=$(MP_PROCESSOR_OPTION)  -frtti -fexceptions -fno-check-new -fenforce-eh-specs -MMD -MF "${OBJECTDIR}/library/stringlib.o.d" -o ${OBJECTDIR}/library/stringlib.o library/stringlib.cpp  
	
${OBJECTDIR}/Tasks/LEDtoggletask.o: Tasks/LEDtoggletask.cpp  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/Tasks 
	@${RM} ${OBJECTDIR}/Tasks/LEDtoggletask.o.d 
	@${RM} ${OBJECTDIR}/Tasks/LEDtoggletask.o 
	@${FIXDEPS} "${OBJECTDIR}/Tasks/LEDtoggletask.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CPPC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c++ -c -mprocessor=$(MP_PROCESSOR_OPTION)  -frtti -fexceptions -fno-check-new -fenforce-eh-specs -MMD -MF "${OBJECTDIR}/Tasks/LEDtoggletask.o.d" -o ${OBJECTDIR}/Tasks/LEDtoggletask.o Tasks/LEDtoggletask.cpp  
	
${OBJECTDIR}/Main.o: Main.cpp  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/Main.o.d 
	@${RM} ${OBJECTDIR}/Main.o 
	@${FIXDEPS} "${OBJECTDIR}/Main.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CPPC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c++ -c -mprocessor=$(MP_PROCESSOR_OPTION)  -frtti -fexceptions -fno-check-new -fenforce-eh-specs -MMD -MF "${OBJECTDIR}/Main.o.d" -o ${OBJECTDIR}/Main.o Main.cpp  
	
else
${OBJECTDIR}/Bluetooth/BTtask.o: Bluetooth/BTtask.cpp  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/Bluetooth 
	@${RM} ${OBJECTDIR}/Bluetooth/BTtask.o.d 
	@${RM} ${OBJECTDIR}/Bluetooth/BTtask.o 
	@${FIXDEPS} "${OBJECTDIR}/Bluetooth/BTtask.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CPPC} $(MP_EXTRA_CC_PRE)  -g -x c++ -c -mprocessor=$(MP_PROCESSOR_OPTION)  -frtti -fexceptions -fno-check-new -fenforce-eh-specs -MMD -MF "${OBJECTDIR}/Bluetooth/BTtask.o.d" -o ${OBJECTDIR}/Bluetooth/BTtask.o Bluetooth/BTtask.cpp  
	
${OBJECTDIR}/Bluetooth/MYUART.o: Bluetooth/MYUART.cpp  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/Bluetooth 
	@${RM} ${OBJECTDIR}/Bluetooth/MYUART.o.d 
	@${RM} ${OBJECTDIR}/Bluetooth/MYUART.o 
	@${FIXDEPS} "${OBJECTDIR}/Bluetooth/MYUART.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CPPC} $(MP_EXTRA_CC_PRE)  -g -x c++ -c -mprocessor=$(MP_PROCESSOR_OPTION)  -frtti -fexceptions -fno-check-new -fenforce-eh-specs -MMD -MF "${OBJECTDIR}/Bluetooth/MYUART.o.d" -o ${OBJECTDIR}/Bluetooth/MYUART.o Bluetooth/MYUART.cpp  
	
${OBJECTDIR}/Display/HX8347D_driver.o: Display/HX8347D_driver.cpp  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/Display 
	@${RM} ${OBJECTDIR}/Display/HX8347D_driver.o.d 
	@${RM} ${OBJECTDIR}/Display/HX8347D_driver.o 
	@${FIXDEPS} "${OBJECTDIR}/Display/HX8347D_driver.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CPPC} $(MP_EXTRA_CC_PRE)  -g -x c++ -c -mprocessor=$(MP_PROCESSOR_OPTION)  -frtti -fexceptions -fno-check-new -fenforce-eh-specs -MMD -MF "${OBJECTDIR}/Display/HX8347D_driver.o.d" -o ${OBJECTDIR}/Display/HX8347D_driver.o Display/HX8347D_driver.cpp  
	
${OBJECTDIR}/Display/draw_interface.o: Display/draw_interface.cpp  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/Display 
	@${RM} ${OBJECTDIR}/Display/draw_interface.o.d 
	@${RM} ${OBJECTDIR}/Display/draw_interface.o 
	@${FIXDEPS} "${OBJECTDIR}/Display/draw_interface.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CPPC} $(MP_EXTRA_CC_PRE)  -g -x c++ -c -mprocessor=$(MP_PROCESSOR_OPTION)  -frtti -fexceptions -fno-check-new -fenforce-eh-specs -MMD -MF "${OBJECTDIR}/Display/draw_interface.o.d" -o ${OBJECTDIR}/Display/draw_interface.o Display/draw_interface.cpp  
	
${OBJECTDIR}/Display/touch_driver.o: Display/touch_driver.cpp  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/Display 
	@${RM} ${OBJECTDIR}/Display/touch_driver.o.d 
	@${RM} ${OBJECTDIR}/Display/touch_driver.o 
	@${FIXDEPS} "${OBJECTDIR}/Display/touch_driver.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CPPC} $(MP_EXTRA_CC_PRE)  -g -x c++ -c -mprocessor=$(MP_PROCESSOR_OPTION)  -frtti -fexceptions -fno-check-new -fenforce-eh-specs -MMD -MF "${OBJECTDIR}/Display/touch_driver.o.d" -o ${OBJECTDIR}/Display/touch_driver.o Display/touch_driver.cpp  
	
${OBJECTDIR}/GUI/STDwindow.o: GUI/STDwindow.cpp  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/GUI 
	@${RM} ${OBJECTDIR}/GUI/STDwindow.o.d 
	@${RM} ${OBJECTDIR}/GUI/STDwindow.o 
	@${FIXDEPS} "${OBJECTDIR}/GUI/STDwindow.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CPPC} $(MP_EXTRA_CC_PRE)  -g -x c++ -c -mprocessor=$(MP_PROCESSOR_OPTION)  -frtti -fexceptions -fno-check-new -fenforce-eh-specs -MMD -MF "${OBJECTDIR}/GUI/STDwindow.o.d" -o ${OBJECTDIR}/GUI/STDwindow.o GUI/STDwindow.cpp  
	
${OBJECTDIR}/GUI/GUItask.o: GUI/GUItask.cpp  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/GUI 
	@${RM} ${OBJECTDIR}/GUI/GUItask.o.d 
	@${RM} ${OBJECTDIR}/GUI/GUItask.o 
	@${FIXDEPS} "${OBJECTDIR}/GUI/GUItask.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CPPC} $(MP_EXTRA_CC_PRE)  -g -x c++ -c -mprocessor=$(MP_PROCESSOR_OPTION)  -frtti -fexceptions -fno-check-new -fenforce-eh-specs -MMD -MF "${OBJECTDIR}/GUI/GUItask.o.d" -o ${OBJECTDIR}/GUI/GUItask.o GUI/GUItask.cpp  
	
${OBJECTDIR}/IlgOS/mem.o: IlgOS/mem.cpp  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/IlgOS 
	@${RM} ${OBJECTDIR}/IlgOS/mem.o.d 
	@${RM} ${OBJECTDIR}/IlgOS/mem.o 
	@${FIXDEPS} "${OBJECTDIR}/IlgOS/mem.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CPPC} $(MP_EXTRA_CC_PRE)  -g -x c++ -c -mprocessor=$(MP_PROCESSOR_OPTION)  -frtti -fexceptions -fno-check-new -fenforce-eh-specs -MMD -MF "${OBJECTDIR}/IlgOS/mem.o.d" -o ${OBJECTDIR}/IlgOS/mem.o IlgOS/mem.cpp  
	
${OBJECTDIR}/IlgOS/messager.o: IlgOS/messager.cpp  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/IlgOS 
	@${RM} ${OBJECTDIR}/IlgOS/messager.o.d 
	@${RM} ${OBJECTDIR}/IlgOS/messager.o 
	@${FIXDEPS} "${OBJECTDIR}/IlgOS/messager.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CPPC} $(MP_EXTRA_CC_PRE)  -g -x c++ -c -mprocessor=$(MP_PROCESSOR_OPTION)  -frtti -fexceptions -fno-check-new -fenforce-eh-specs -MMD -MF "${OBJECTDIR}/IlgOS/messager.o.d" -o ${OBJECTDIR}/IlgOS/messager.o IlgOS/messager.cpp  
	
${OBJECTDIR}/IlgOS/scheduler.o: IlgOS/scheduler.cpp  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/IlgOS 
	@${RM} ${OBJECTDIR}/IlgOS/scheduler.o.d 
	@${RM} ${OBJECTDIR}/IlgOS/scheduler.o 
	@${FIXDEPS} "${OBJECTDIR}/IlgOS/scheduler.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CPPC} $(MP_EXTRA_CC_PRE)  -g -x c++ -c -mprocessor=$(MP_PROCESSOR_OPTION)  -frtti -fexceptions -fno-check-new -fenforce-eh-specs -MMD -MF "${OBJECTDIR}/IlgOS/scheduler.o.d" -o ${OBJECTDIR}/IlgOS/scheduler.o IlgOS/scheduler.cpp  
	
${OBJECTDIR}/library/stringlib.o: library/stringlib.cpp  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/library 
	@${RM} ${OBJECTDIR}/library/stringlib.o.d 
	@${RM} ${OBJECTDIR}/library/stringlib.o 
	@${FIXDEPS} "${OBJECTDIR}/library/stringlib.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CPPC} $(MP_EXTRA_CC_PRE)  -g -x c++ -c -mprocessor=$(MP_PROCESSOR_OPTION)  -frtti -fexceptions -fno-check-new -fenforce-eh-specs -MMD -MF "${OBJECTDIR}/library/stringlib.o.d" -o ${OBJECTDIR}/library/stringlib.o library/stringlib.cpp  
	
${OBJECTDIR}/Tasks/LEDtoggletask.o: Tasks/LEDtoggletask.cpp  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/Tasks 
	@${RM} ${OBJECTDIR}/Tasks/LEDtoggletask.o.d 
	@${RM} ${OBJECTDIR}/Tasks/LEDtoggletask.o 
	@${FIXDEPS} "${OBJECTDIR}/Tasks/LEDtoggletask.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CPPC} $(MP_EXTRA_CC_PRE)  -g -x c++ -c -mprocessor=$(MP_PROCESSOR_OPTION)  -frtti -fexceptions -fno-check-new -fenforce-eh-specs -MMD -MF "${OBJECTDIR}/Tasks/LEDtoggletask.o.d" -o ${OBJECTDIR}/Tasks/LEDtoggletask.o Tasks/LEDtoggletask.cpp  
	
${OBJECTDIR}/Main.o: Main.cpp  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/Main.o.d 
	@${RM} ${OBJECTDIR}/Main.o 
	@${FIXDEPS} "${OBJECTDIR}/Main.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CPPC} $(MP_EXTRA_CC_PRE)  -g -x c++ -c -mprocessor=$(MP_PROCESSOR_OPTION)  -frtti -fexceptions -fno-check-new -fenforce-eh-specs -MMD -MF "${OBJECTDIR}/Main.o.d" -o ${OBJECTDIR}/Main.o Main.cpp  
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/Fernbedienung.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CPPC} $(MP_EXTRA_LD_PRE)  -mdebugger -D__MPLAB_DEBUGGER_PK3=1 -mprocessor=$(MP_PROCESSOR_OPTION)  -o dist/${CND_CONF}/${IMAGE_TYPE}/Fernbedienung.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}           -mreserve=data@0x0:0x1FC -mreserve=boot@0x1FC02000:0x1FC02FEF -mreserve=boot@0x1FC02000:0x1FC024FF  -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),--defsym=__MPLAB_DEBUG=1,--defsym=__DEBUG=1,--defsym=__MPLAB_DEBUGGER_PK3=1,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map"
	
else
dist/${CND_CONF}/${IMAGE_TYPE}/Fernbedienung.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CPPC} $(MP_EXTRA_LD_PRE)  -mprocessor=$(MP_PROCESSOR_OPTION)  -o dist/${CND_CONF}/${IMAGE_TYPE}/Fernbedienung.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}          -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map"
	${MP_CC_DIR}\\xc32-bin2hex dist/${CND_CONF}/${IMAGE_TYPE}/Fernbedienung.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} 
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/default
	${RM} -r dist/default

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
