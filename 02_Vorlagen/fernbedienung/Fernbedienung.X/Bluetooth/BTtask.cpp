//------------------------------------------------------------
// Description:
//  Bluetooth functions
//
// Author:
//  Christoph Ilg
//
// Date:
//  24.03.2014
//------------------------------------------------------------


#ifndef BTTASK_CPP
#define BTTASK_CPP

#include "BTtask.h"


bool BTtask::OK_received;
unsigned short BTtask::doping;
unsigned short BTtask::timeout_CTR;
unsigned char BTtask::nowstate;
unsigned char BTtask::message_length;
unsigned char *BTtask::message_ptr;
unsigned char BTtask::address_datalength;
unsigned char BTtask::address_data[18];
unsigned char BTtask::name_datalength;
unsigned char BTtask::name_data[20];


void BTtask::init()
{
	nowstate = 0;
	timeout_CTR = 0;
	OK_received = false;
        doping = 0;
        address_data[18] = '\0';
#ifdef DEVICE_TOSETUP
        //Gerät auf Master +STWMOD=1
        //Pin setzen +STPIN=1975
        UARTsend((unsigned char*)"\r\n+STAUTO=1\r\n");
#endif
}


void BTtask::run()
{
	//timeout counter
	timeout_CTR++;
	if ((nowstate > 2)||(nowstate==1))
        {
            if (timeout_CTR >= TIMEOUT_MAXCTR) nowstate = 0;
        }
        else 
        {
            if (timeout_CTR >= (3*TIMEOUT_MAXCTR)) nowstate = 0;
        }
        //echo counter
        if (nowstate==4)
        {
            doping++;
            if (doping >= TIMEFOR_ECHO)
            {
                doping = 0;
                UARTsend((unsigned char*)"\r\necho\r\n");
            }
        }

	//Nachricht empfangen - ist was da?
        long message;
        if (messager::get(&message,CHANNELFORUART))
        {
            message_length = mem::getadr(message);
            message_ptr = mem::getptr(message);
            //Echo Nachricht wurde empfangen
            if (matching(message_ptr,4,(unsigned char*)"echo"))
            {
                nowstate = 4;
                timeout_CTR = 0;
                mem::freeadr(message);
                return;
            }
            //Nachricht zum weiterleiten wurde empfangen, muss mit ! beginnen
            if (nowstate==4)
            {
                if (*message_ptr == '!')
                {
					//prüfe, ob Nachricht gültig ist
					if (!stringlib::unpackage_BT_message(message_ptr, message_length)) return;
					unsigned char *ptr = stringlib::getbufferptr();
					//sende Nachricht an entsprechendes Postfach
					short memaddr = mem::newadr(ptr[3]);
					unsigned char *memptr = mem::getptr(memaddr);
					for (unsigned char i = 0; i < ptr[3]; i++)
					{
						memptr[i] = ptr[i + 4];
					}
					messager::put(memaddr, ptr[2]);
                }
                timeout_CTR=0;
                mem::freeadr(message);
                return;
            }
            //Error wurde empfangen
            if (matching(message_ptr,5,(unsigned char*)"ERROR"))
            {
                nowstate = 0;
                mem::freeadr(message);
                return;
            }
            //BTstate wurde empfangen
            if (matching(message_ptr,9,(unsigned char*)"+BTSTATE:"))
            {
                nowstate = *(message_ptr+9)-'0';
                timeout_CTR = 0;
                mem::freeadr(message);
                return;
            }
            //OK wurde empfangen
            if (matching(message_ptr,2,(unsigned char*)"OK"))
            {
                OK_received = true;
                timeout_CTR = 0;
                mem::freeadr(message);
                return;
            }
            //RTINQ wurd empfangen
            if (matching(message_ptr,7,(unsigned char*)"+RTINQ="))
            {
                timeout_CTR = 0;
                for (address_datalength = 0;address_datalength<17;address_datalength++)
                {
                    unsigned char data = *(message_ptr+address_datalength+7);
                    if (data==';') break;
                    address_data[address_datalength] = data;
                }
                address_datalength++;
                name_datalength = message_length - 7 - address_datalength;
                if (name_datalength > 20) name_datalength = 20;
                for (unsigned char i = 0;i<name_datalength;i++)
                {
                    name_data[i] = *(message_ptr+address_datalength+7+i);
                }
                //prüfe, ob der Name des Gerätes passend, wenn ja verbinde
                if (matching(name_data,name_datalength,(unsigned char*)NAME_TO_CONNECT))
                {
                    if (address_datalength < 17) address_data[address_datalength] = '\0';
                    UARTsend((unsigned char*)"\r\n+CONN=");
                    UARTsend((unsigned char*)address_data);
                    UARTsend((unsigned char*)"\r\n");
                }
                timeout_CTR = 0;
                mem::freeadr(message);
                return;
            }
            //Pin-Anforderung wurde empfangen
            if (matching(message_ptr,6,(unsigned char*)"+INPIN"))
            {
                UARTsend((unsigned char*)PIN_FOR_CONNECTION);
                timeout_CTR = 0;
                mem::freeadr(message);
                return;
            }
            mem::freeadr(message);
        }
        else if (nowstate == 0)
        {
        // -------------------------------------------
        // Hier evtl. init() einbauen, da ein Verbindungsaufbau nur nach neustart möglich ist!
        // Aber warum bricht die Verbindung ab?
            //sende INQ nachricht
            UARTsend((unsigned char*)"\r\n+INQ=1\r\n");
            nowstate = 1;
            timeout_CTR = 0;
        }
        //ist eine Nachricht zu senden?
	if (nowstate == 4)
	{
		long message2;
		if (messager::get(&message2, CHANNELFORBT_TOSEND))
		{
			unsigned char message_length2 = mem::getadr(message2);
			unsigned char *message_ptr2 = mem::getptr(message2);
			//verpacke für versand, erstes unsigned char ist der Zielkanal
			stringlib::package_BT_message(&(message_ptr2[1]), message_length2 - 1, message_ptr2[0]);
			mem::freeadr(message2);
			//versende
			UARTsend((unsigned char*)"\r\n");
			UARTsend((unsigned char*)stringlib::getbufferptr());
			UARTsend((unsigned char*)"\r\n");
		}
	}
}


bool BTtask::matching(unsigned char *instring, unsigned char inlength, unsigned char *vglstring)
{
    for (unsigned char i = 0; i < inlength; i++)
    {
        if (*(instring+i) != *(vglstring+i)) return false;
    }
    if (*(vglstring+inlength)!='\0') return false;
    return true;
}


unsigned char BTtask::state()
{
    return nowstate;
}

#endif
