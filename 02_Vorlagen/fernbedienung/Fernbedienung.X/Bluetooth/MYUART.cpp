//------------------------------------------------------------
// Description:
//  UART functions
//
// Author:
//  Christoph Ilg
//
// Date:
//  24.03.2014
//------------------------------------------------------------


#ifndef MYUART_CPP
#define MYUART_CPP

#include "MYUART.h"

#define	GetSystemClock() 			(80000000ul)
#define	GetPeripheralClock()		(GetSystemClock()/(1 << OSCCONbits.PBDIV))
#define	GetInstructionClock()		(GetSystemClock())

extern "C"
{
        //Zwischenspeicheradresse
        short UARTtempmem;
        unsigned char *UARTtempmemptr;
        unsigned char UARTwriteptr;

	//UART initialisieren
	void UARTinit()
	{
		//setze RTS
		//mPORTAClearBits(BIT_7);
		UARTConfigure(UART1, UART_ENABLE_PINS_TX_RX_ONLY);
		UARTSetFifoMode(UART1, (UART_FIFO_MODE)(UART_INTERRUPT_ON_TX_NOT_FULL | UART_INTERRUPT_ON_RX_FULL));//UART_INTERRUPT_ON_RX_NOT_EMPTY));
		UARTSetLineControl(UART1, (UART_LINE_CONTROL_MODE)(UART_DATA_SIZE_8_BITS | UART_PARITY_NONE | UART_STOP_BITS_1));
		//Setze Baud-Rate
		UARTSetDataRate(UART1, GetPeripheralClock(), 38400);
		UARTEnable(UART1, (UART_ENABLE_MODE)(UART_ENABLE_FLAGS(UART_PERIPHERAL | UART_RX | UART_TX)));
		//Configure UART1 RX Interrupt
		INTEnable((INT_SOURCE)INT_SOURCE_UART_RX(UART1), INT_ENABLED);
		INTSetVectorPriority((INT_VECTOR)INT_VECTOR_UART(UART1), INT_PRIORITY_LEVEL_2);
		INTSetVectorSubPriority((INT_VECTOR)INT_VECTOR_UART(UART1), INT_SUB_PRIORITY_LEVEL_0);
                //initialisiere Zwischenspeicheradresse
                UARTwriteptr = 0;
                UARTtempmem = mem::newadr(UARTTEMPLENGTH);
                UARTtempmemptr = mem::getptr(UARTtempmem);
	}

        
	//Interuptroutine f�r UART
	void __ISR(_UART1_VECTOR, IPL2SOFT) IntUART1Handler(void)
	{
		// RX interrupt?
		if (INTGetFlag((INT_SOURCE)INT_SOURCE_UART_RX(UART1)))
		{
                        unsigned char received = U1RXREG;
                        if ((received != '\r'))
                        {
                            if (received == '\n')
                            {
                                if (UARTwriteptr != 0)
                                {
                                    //schicke ab
                                    mem::shortenlength(UARTtempmem,UARTwriteptr);
                                    messager::put(UARTtempmem,CHANNELFORUART);
                                    //hole neuen Speicherbereich
                                    UARTwriteptr = 0;
                                    UARTtempmem = mem::newadr(UARTTEMPLENGTH);
                                    UARTtempmemptr = mem::getptr(UARTtempmem);
                                }
                            }
                            else
                            {
                                    *(UARTtempmemptr + UARTwriteptr) = received;
                                    UARTwriteptr++;
                                    if (UARTwriteptr == UARTTEMPLENGTH) UARTwriteptr = 0;
                            }
                        }
			// Clear the RX interrupt Flag
			INTClearFlag((INT_SOURCE)INT_SOURCE_UART_RX(UART1));
		}
		//TX interrupt?
		if (INTGetFlag((INT_SOURCE)INT_SOURCE_UART_TX(UART1)))
		{
			INTClearFlag((INT_SOURCE)INT_SOURCE_UART_TX(UART1));
		}
	}


        //sendet UART-Daten solange bis 0 in char-array
        void UARTsend(unsigned char *buffer)
        {
                //l�sche RTS
                //mPORTASetBits(BIT_7);
                while(true)
                {
                    while(!UARTTransmitterIsReady(UART1))
                    {};
                    if (*buffer == '\0') break;
                    UARTSendDataByte(UART1, *buffer);
                    buffer++;
                }
               /*while(!UARTTransmissionHasCompleted(UART1))
                {};*/
                //setze RTS
                //mPORTAClearBits(BIT_7);
        }
}



#endif
