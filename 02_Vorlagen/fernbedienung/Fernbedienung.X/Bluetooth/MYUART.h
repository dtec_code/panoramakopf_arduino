//------------------------------------------------------------
// Description:
//  UART functions
//
// Author:
//  Christoph Ilg
//
// Date:
//  24.03.2014
//------------------------------------------------------------


#ifndef MYUART_H
#define MYUART_H

#include <GenericTypeDefs.h>
#include <p32xxxx.h>
#include <plib.h>
#include "../IlgOS/mem.h"
#include "../IlgOS/messager.h"
#include "../IlgOS/channeldefs.h"

#define UARTTEMPLENGTH 64

extern "C"
{
        //UART initialisieren
	void UARTinit();

        //sendet UART-Daten solange bis 0 in char-array
        void UARTsend(unsigned char *buffer);
}



#endif
