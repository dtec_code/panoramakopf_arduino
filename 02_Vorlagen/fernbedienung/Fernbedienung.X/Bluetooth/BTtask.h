//------------------------------------------------------------
// Description:
//  Bluetooth functions
//
// Author:
//  Christoph Ilg
//
// Date:
//  24.03.2014
//------------------------------------------------------------


#ifndef BTTASK_H
#define BTTASK_H

#include <GenericTypeDefs.h>
#include <p32xxxx.h>
#include <plib.h>
#include "../IlgOS/mem.h"
#include "../IlgOS/messager.h"
#include "../IlgOS/channeldefs.h"
#include "MYUART.h"
#include "../GUI/STDwindow.h"
#include "../library/stringlib.h"

#define TIMEOUT_MAXCTR 1000 //bei 20ms Taskzeit alle 10s-> 500
#define TIMEFOR_ECHO 50 //gibt an, wie oft Echo gesendet wird -> 5s
#define NAME_TO_CONNECT "Panoramakopf" //Name des Ger�tes, zu dem Verbindung aufgebaut werden soll
#define PIN_FOR_CONNECTION "\r\n+RTPIN=1975\r\n" //Pin nach Anforderung
//#define DEVICE_TOSETUP

//geht dies sowohl als auch?
class BTtask
{
private:
	//ein OK wurde received
	static bool OK_received;
        //eine Aktion ist durchzuf�hren
        static unsigned short doping;
	//counter um timeout zu erzielen
	static unsigned short timeout_CTR;
	//aktueller Empfangsstatus
	static unsigned char nowstate;
        //l�nge der empfangenen Nachricht
        static unsigned char message_length;
        //zeiger auf die empfangene Nachricht
        static unsigned char *message_ptr;
        //Adresse zu verbinden
        static unsigned char address_datalength;
        static unsigned char address_data[18];
        //Name des Ger�tes - zu verbinden
        static unsigned char name_datalength;
        static unsigned char name_data[20];



        //gibt an, ob die zweit strings zueinander passen
        static bool matching(unsigned char *instring, unsigned char inlength,
                             unsigned char *vglstring);
//echoreceived/send
	//Status 1: starte inquiring
	//static void do_phase1();
	/*//Status 2: warte auf ger�t
	static void do_phase2();
	//Status 3: */

public:
	//zur Initialisierung aufrufen
	static void init();
	//BTtask zum aufrufen
	static void run();
        //gibt status zur�ck
        static unsigned char state();


};




#endif
