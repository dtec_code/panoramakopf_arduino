# README - Panoramakopf Arduino #

##### Use Arduino IDE 1.5.8 Beta -> http://arduino.cc/download.php?f=/arduino-1.5.8-windows.exe #####

##### Libraries #####
Alle Ordner müssen EINZELN in der IDE includiert werden. Anderst ist eines Funktion nicht garantiert!
Detaillierte Beschreibung der einzelnen Ordner unter Downloads.

##### Group Members #####
+ Sabine Schneider  HTW Aalen
- Kirill Wetoschkin HTW Aalen
+ Daniel Bauer HTW Aalen

##### Description: https://bitbucket.org/dtec_code/panoramakopf_arduino/downloads #####