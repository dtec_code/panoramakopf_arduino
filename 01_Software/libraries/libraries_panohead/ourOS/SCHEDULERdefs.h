//------------------------------------------------------------
// Description:
//  Die Definition der Tasks
//
// Author:
//  Christoph Ilg
//
// Date:
//  24.03.2014
//------------------------------------------------------------


/*
Protokoll des Nachrichtenschickens an channel CHANNELFORSCHEDULER:
- wenn die Nachricht zwischen -1 und -256 ist wird der entsprechende once-task ausgef�hrt!
  als Betrag des Wertes -1
- wenn die Nachricht postiven wert hat, wird unter dem Betrag dieses Wertes als mem-
  Adresse ein 6er-Wert erwartet und entsprechend gehandelt:
  - Byte 0: bei 0 auf high-priority Tasks bezogen, bei 1 auf low-priority
  - Byte 1: Nummer des Tasks
  - Byte 2: h�chstwertigstes Byte der Wiederholzeit
  - Byte 3:
  - Byte 4:
  - Byte 5:
  - Byte 6: niederwertigstes Byte der Wiederholzeit
  sende Wiederholzeit 0 zum deaktivieren
  (Achtung - die letzten laufenden Task lassen sich nicht deaktivieren)
*/


//es werden erst die mit hoher, dann die mit niederer Priorit�t ausgef�hrt
//-> dann die, die schon am l�ngsten warten!
//Kombination aus Priorit�ts und warte-scheduling
//dieses Design unterst�tzt klare Struktur des Systems!!


#ifndef SCHEDULERDEFS_H
#define SCHEDULERDEFS_H

//Die Dateien f�r die auszuf�hrenden Tasks
//#include "..\tasks\ledtasks.h"
//#include "..\tasks\terminaltask.h"
//#include "Panoramakopfcontroller.h"
//#include "..\Bluetooth\BTtask.h"

//Allgemeine Definitionen
#define NUMBEROFTASKS_LOWPRIO 6 //muss immer mindestens 1 sein
#define NUMBEROFTASKS_HIGHPRIO 6 //muss immer mindestens 1 sein


//Die Wiederholtaskaktionen
#define TASK_HIGHPRIO_1_ACTION Panoramakopfcontroller::Motorrun(); 
#define TASK_HIGHPRIO_2_ACTION BTtask::serialtask();
#define TASK_HIGHPRIO_3_ACTION BTtask::run();
#define TASK_HIGHPRIO_4_ACTION PTPtask::run();
#define TASK_HIGHPRIO_5_ACTION break;
#define TASK_LOWPRIO_1_ACTION Panoramakopfcontroller::run();
#define TASK_LOWPRIO_2_ACTION LEDtask::run();
#define TASK_LOWPRIO_3_ACTION break;
#define TASK_LOWPRIO_4_ACTION break;
#define TASK_LOWPRIO_5_ACTION break;

//Die Taskwiederholzeiten
#define TASK_HIGHPRIO_1_CYCLETIME 400;  //f�r den Motorentask!!
#define TASK_HIGHPRIO_2_CYCLETIME 2000; //2000;  //z.B. f�r BT 100000
#define TASK_HIGHPRIO_3_CYCLETIME 20000;
#define TASK_HIGHPRIO_4_CYCLETIME 2000;
#define TASK_HIGHPRIO_5_CYCLETIME 0;
#define TASK_LOWPRIO_1_CYCLETIME 10000;
#define TASK_LOWPRIO_2_CYCLETIME 20000;
#define TASK_LOWPRIO_3_CYCLETIME 0;
#define TASK_LOWPRIO_4_CYCLETIME 0;
#define TASK_LOWPRIO_5_CYCLETIME 0;

//Die Einmaltaskaktionen
#define TASK_ONCE_0_ACTION break;
#define TASK_ONCE_1_ACTION break;
#define TASK_ONCE_2_ACTION break;
#define TASK_ONCE_3_ACTION break;
#define TASK_ONCE_4_ACTION break;


#endif