//------------------------------------------------------------
// Description:
//  Die Definition der Gr��e des Speichersystems
//
// Author:
//  Christoph Ilg
//
// Date:
//  24.03.2014
//------------------------------------------------------------

#ifndef MEMDEFS_H
#define MEMDEFS_H


#define DATATYP unsigned char
#define DATATYP_LENGTH unsigned char
#define LENGTH_SMALL 16
#define NUMBER_SMALL 128 
#define LENGTH_MEDIUM 64
#define NUMBER_MEDIUM 64 
#define LENGTH_BIG 256
#define NUMBER_BIG 32 


#endif