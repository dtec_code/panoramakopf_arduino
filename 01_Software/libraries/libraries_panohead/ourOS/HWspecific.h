//------------------------------------------------------------
// Description:
//  Hardwarespezifische Definitionen (z.B. arduino vs 
//  Mikromedia)
//
// Author:
//  Christoph Ilg
//
// Date:
//  24.03.2014
//------------------------------------------------------------

#ifndef HWSPECIFIC_H
#define HWSPECIFIC_H

//Methode, um die aktuelle Zeit zu bekommen
//sowie evtl. Multiplier
#define MIKROSECONDSMULTIPLIER 1 //80 bei Mikromedia
#define GETNOWTIME micros();
#define INTERRUPT_ENABLE_METHOD interrupts();
#define INTERRUPT_DISABLE_METHOD noInterrupts();

//Nullpointer
#define SPECIFICNULLPTR 0 //null
#endif