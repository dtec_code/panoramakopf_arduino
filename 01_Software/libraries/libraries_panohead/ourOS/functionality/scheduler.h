//------------------------------------------------------------
// Description:
//  Der Scheduler, die auszuf�hrenden Tasks in SCHEDULERdefs
//  festlegen
//
// Author:
//  Christoph Ilg
//
// Date:
//  24.03.2014
//------------------------------------------------------------

/*
Protokoll des Nachrichtenschickens an channel CHANNELFORSCHEDULER:
- wenn die Nachricht zwischen -1 und -256 ist wird der entsprechende once-task ausgef�hrt!
  als Betrag des Wertes -1
- wenn die Nachricht postiven wert hat, wird unter dem Betrag dieses Wertes als mem-
  Adresse ein 6er-Wert erwartet und entsprechend gehandelt:
  - Byte 0: bei 0 auf high-priority Tasks bezogen, bei 1 auf low-priority
  - Byte 1: Nummer des Tasks
  - Byte 2: h�chstwertigstes Byte der Wiederholzeit
  - Byte 3:
  - Byte 4:
  - Byte 5:
  - Byte 6: niederwertigstes Byte der Wiederholzeit
  sende Wiederholzeit 0 zum deaktivieren
  (Achtung - die letzten laufenden Task lassen sich nicht deaktivieren)
*/


//es werden erst die mit hoher, dann die mit niederer Priorit�t ausgef�hrt
//-> dann die, die schon am l�ngsten warten!
//Kombination aus Priorit�ts und warte-scheduling
//dieses Design unterst�tzt klare Struktur des Systems!!


#ifndef SCHEDULER_H
#define SCHEDULER_H

#include <Arduino.h>
#include "mem.h"
#include "messager.h"
#include "../SCHEDULERdefs.h"
#include "eventsys.h"


class Scheduler
{
private:
	static unsigned long tasktimes_lowprio[NUMBEROFTASKS_LOWPRIO];
	static unsigned long tasktimes_highprio[NUMBEROFTASKS_HIGHPRIO];
	static unsigned long nexttime_lowprio[NUMBEROFTASKS_LOWPRIO];
	static unsigned long nexttime_highprio[NUMBEROFTASKS_HIGHPRIO];
	//die aktuelle Zeit
	static unsigned long nowtime;
	//nexttask-Variablen
	static unsigned char nexttask_highprio;
	static unsigned char nexttask_lowprio;
	static unsigned long nexttasktime_highprio;
	static unsigned long nexttasktime_lowprio;
	//f�hrt tasks mit niedriger Priorit�t mit index taskindex aus
	static inline void runtask_lowprio(unsigned char taskindex);
	//f�hrt tasks mit hoher Priorit�t mit index taskindex aus
	static inline void runtask_highprio(unsigned char taskindex);
	//f�hrt einmal-tasks mit index taskindex aus
	static inline void runtask_once(unsigned char taskindex);
	//aktualisiert nowtime, sichert gegen �berlauf ab!
	static inline void actualize_nowtime();
	//behandle zeit�berlauf
	static void zeitueberlauf();
	//falls Zeitpunkt da, wird ein Task hoher Priorit�t ausgef�hrt,
	//dann wird true zur�ckgegeben und die nexttaskvalues aktualisiert
	//sonst false (auf aktuelle Zeit wird sich verlassen!)
	static inline bool check_highprio();
	//arbeitet Nachrichten ab, falls es etwas zu tun gab wird true
	//zur�ck gegeben, sonst false
	static inline bool check_messages();
	//falls Zeitpunkt da, wird ein Task niederer Priorit�t ausgef�hrt,
	//dann wird true zur�ckgegeben und die nexttaskvalues aktualisiert
	//sonst false (auf aktuelle Zeit wird sich verlassen!)
	static inline bool check_lowprio();

public:
	//l�sst den Scheduler laufen
	static void run();
};


#endif
