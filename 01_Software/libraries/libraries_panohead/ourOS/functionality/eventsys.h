//------------------------------------------------------------
// Description:
//  Eventsystem
//
// Author:
//  Christoph Ilg
//
// Date:
//  24.03.2014
//------------------------------------------------------------


#ifndef EVENTSYS_H
#define EVENTSYS_H

#include <Arduino.h>
#include "../EVENTSdefs.h"
#include "../HWspecific.h"


typedef void(*VoidFnct) (void);

class eventsys
{
private:
	//speichert die auszuf�hrenden Aktionen bei auftreten des Events
	static VoidFnct fktptr[EVENTS_MAXNUMBER];
	//ein event f�r den channel ist auszuf�hren, wenn von 1 auf 0 runtergez�hlt wird
	//bei 0 nichts zu tun
	static unsigned short eventdoctr[EVENTS_MAXNUMBER];
public:
	//initialisiert eventsystem
	static void init();
	//attached Funktion todo an event eventname
	static void attach(unsigned char eventname, VoidFnct todo);
	//l�se event sofort aus und arbeitet es ab
	static void raise_now(unsigned char eventname);
	//l�se event nach raisectr eventsys-Zyklen im Task (hohe Priorit�t) aus (in 10 ms), 
	//eins l�st beim n�chsten mal aus, null deaktiviert
	//bei negativem raisectr gleiches Verhalten, jedoch Ausf�hrung mit niedriger Priorit�t
	static void raise_task(unsigned char eventname, short raisectr);
	//lasse eventtask laufen muss im Scheduler laufen (alle 10ms) als hohe Priorit�t
	static void run_highprio();
	//lasse eventtask laufen muss im Scheduler laufen (alle 10ms) als niedrige Priorit�t
	static void run_lowprio();
};


#endif
