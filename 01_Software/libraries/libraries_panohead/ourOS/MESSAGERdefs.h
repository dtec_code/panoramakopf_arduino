//------------------------------------------------------------
// Description:
//  Die Definition der Messagekan�le
//
// Author:
//  Christoph Ilg
// Editiert:
//  Daniel Bauer
//
// Date:
//  24.03.2014
//------------------------------------------------------------

#ifndef MESSAGERDEFS_H
#define MESSAGERDEFS_H


//Algemeine Definitionen
#define CHANNELS 6
#define MAXMESSAGES 32
#define MESSAGEDATATYP long

//Definitionen der Kanaele
#define CHANNELFORSCHEDULER 0           //fuer Scheduler
#define CHANNELFORUART 1                //fuer UART
#define CHANNELFORBT_TOSEND 2           //Kanal, an den die zu sendenden BT-Nachrichten kommen
#define CHANNELFORBT_PKOPFDATAIN 3      //Kanal, der die BT-Nachrichtendaten f�r den Panoramakopfcontroller aufnimmt
#define CHANNELFORBT_PKOPFCOMIN 4       //Kanal, der die BT-Nachrichtenbefehle f�r den Panoramakopfcontroller aufnimmt




#endif