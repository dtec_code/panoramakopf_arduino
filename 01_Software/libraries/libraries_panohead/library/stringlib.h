//------------------------------------------------------------
// Description:
//  Funktionen zum Umgang mit Zeichenketten>
//
// Author:
//  Christoph Ilg
//
// Date:
//  24.03.2014
//------------------------------------------------------------

		
	/*
	Verpackungsvorgang:
	1) Header erstellen:
		Byte 0: '!'
		Byte 1: Pr�fbyte 0, h�herwertiges Byte der Summe aller Bytes ab Byte 3
		Byte 2: Pr�fbyte 1, niederwertiges Byte der Summe aller Bytes ab Byte 3
		Byte 3: der Zielkanal
		Byte 4: L�nge der Nutzlast
	2) Ersetzungen um  mit '\0' abschliessende Zeichekette ohne '\r' und '\n' zu haben:
		- zu "-e"  (45)
		'\0' zu "-0"  (0)
		'\r' zu "-r"  (13)
		'\n' zu "-n"  (10)
	*/

#ifndef STRINGLIB_H
#define STRINGLIB_H

#include <Arduino.h>

class stringlib
{
private:
	static unsigned char buffer[1+8+240+1];
	static unsigned char bufferlength;
	//die Daten zum versenden bereit in Sendbuffer gegeben
	//dabei werden die einzelnen Werte verpackt
	static void packinbuffer(unsigned char inval);
public:
	//verpackt die Daten aus inptr mit der L�nge inlength zur Versendung per BT an Empf�ngerkanal receivechannel (darf nicht gr��er 120 sein!)
	//und steckt sie in sendbuffer
	//m�ssen noch in \r\n eingepackt werden und terminiert mit '\0'
	static void package_BT_message(unsigned char *inptr, unsigned char inlength, unsigned char receivechannel);
	//entpackt die Daten aus inptr mit der L�nge inlength in buffer und gibt an, ob es g�ltig war
	//dabei werden pr�fbyte 0 und 1, zielkanal und l�nge mit �bersetzt
	static bool unpackage_BT_message(unsigned char *inptr, unsigned char inlength);
	//gibt zeiger auf buffer zur�ck (zeichenkette terminiert mit \'0' beim versenden),
	//sonst frage noch nach l�nge
	static unsigned char *getbufferptr();
	//gibt l�nge des buffers zur�ck
	static unsigned char getbufferlength();




};



#endif
