//------------------------------------------------------------
// Description:
//  Funktionen zum Umgang mit Zeichenketten
//
// Author:
//  Christoph Ilg
//
// Date:
//  24.03.2014
//------------------------------------------------------------


#ifndef STRINGLIB_CPP
#define STRINGLIB_CPP

#include "stringlib.h"


unsigned char stringlib::buffer[1+8+240+1];
unsigned char stringlib::bufferlength;

void stringlib::packinbuffer(unsigned char inval)
{
	switch(inval)
	{
	case 0: //'\0'
		buffer[bufferlength++] = '-';
		buffer[bufferlength++] = '0';
		break;
	case 10: //'\n'
		buffer[bufferlength++] = '-';
		buffer[bufferlength++] = 'n';
		break;
	case 13: //'\r'
		buffer[bufferlength++] = '-';
		buffer[bufferlength++] = 'r';
		break;
	case 45: //'\-'
		buffer[bufferlength++] = '-';
		buffer[bufferlength++] = 'e';
		break;
	default:
		buffer[bufferlength++] = inval;
	}
}

// Sendemethode f�r Bluetooth
// intpr Zeiger auf die Nachricht
// inlength L�nge der Nachricht
// receivechannel aus Messagerdefs.h fuer senden an Handy bsp. 3
void stringlib::package_BT_message(unsigned char *inptr, unsigned char inlength, unsigned char receivechannel)
{
	if (inlength > 120) inlength = 120;
	//ermittle pr�fsumme
	unsigned short pruefsumme = 0;
	pruefsumme += receivechannel;
	pruefsumme += inlength;
	for (unsigned char i=0;i<inlength;i++)
	{
		pruefsumme += *(inptr+i);
	}
	unsigned char pwert0 = pruefsumme/256;
	unsigned char pwert1 = pruefsumme%256;
	bufferlength = 0;

	//erstelle buffer
	packinbuffer('!');
	packinbuffer(pruefsumme/256);
	packinbuffer(pruefsumme%256);
	packinbuffer(receivechannel);
	packinbuffer(inlength);
	for (unsigned char i=0;i<inlength;i++)
	{
		packinbuffer(*(inptr+i));
	}
	buffer[bufferlength++] = '\0';
}

bool stringlib::unpackage_BT_message(unsigned char *inptr, unsigned char inlength)
{
	//entpacke in buffer
	if (*inptr != '!') return false;
	bufferlength = 0;
	for (unsigned char i = 1; i<inlength; i++)
	{
		if (*(inptr + i) == '-')
		{
			i++;
			if (*(inptr + i) == '0') buffer[bufferlength++] = '\0';
			if (*(inptr + i) == 'e') buffer[bufferlength++] = '-';
			if (*(inptr + i) == 'r') buffer[bufferlength++] = '\r';
			if (*(inptr + i) == 'n') buffer[bufferlength++] = '\n';
		}
		else
			buffer[bufferlength++] = *(inptr + i);
	}
#ifdef DEBUG_OUT
    Serial.print("-> BT: unpack Msg: ");
    for (unsigned char i = 0; i<inlength; i++)
    {
        Serial.print(*(inptr + i));
        Serial.print("-");
    }
    Serial.print("\n");
#endif
    //pruefe gueltigkeit
	if (bufferlength > 124) return false;
	unsigned short pruefsumme = 0;
	unsigned short vglpruefsumme = ((unsigned short)buffer[0] * 256) + ((unsigned short)buffer[1]);
	for (unsigned char i = 2; i<bufferlength; i++)
	{
		pruefsumme +=buffer[i];
	}
	if (pruefsumme != vglpruefsumme) return false;
	if (buffer[3] != bufferlength - 4) return false;
	return true;
}

unsigned char *stringlib::getbufferptr()
{
	return buffer;
}

unsigned char stringlib::getbufferlength()
{
	return bufferlength;
}


#endif
