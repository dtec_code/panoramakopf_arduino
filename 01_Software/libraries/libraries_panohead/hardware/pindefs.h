//------------------------------------------------------------
// Description:
//  Die Definition der verwendeten Pins
//
// Author:
//  Christoph Ilg
// Editiert:
//  Daniel Bauer
//
// Date:
//  24.03.2014
//------------------------------------------------------------

#ifndef PINDEFS_H
#define PINDEFS_H


//Positionssensor
#define PIN_POS_SIG A0
#define PIN_POS_EN 52

//Optokoppler Kameraverbindung
#define PIN_OPT_SHUTTER 51
#define PIN_OPT_FOKUS 53

//LEDs
#define PIN_LED_STATUS 46
#define PIN_LED_BT 50
#define PIN_LED_WARN 48

//Motor 1
#define PIN_M1_EN 30
#define PIN_M1_DIR 32
#define PIN_M1_CLK 2
#define PIN_M1_ABSENK 34

//Motor 2
#define PIN_M2_EN 36
#define PIN_M2_DIR 38
#define PIN_M2_CLK 3
#define PIN_M2_ABSENK 40

//Bluetooth
#define PIN_BT_RX 14//TX3 von Board
#define PIN_BT_TX 15 //RX3 von Board
#define PIN_BT_CTS 22
#define PIN_BT_RTS 24
#define PIN_BT_RESET 28
#define PIN_BT_PIO0 26

#endif