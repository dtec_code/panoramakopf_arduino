//------------------------------------------------------------
// Description:
//  Der Controller fuer den Panoramakopf
//
// Author:
//  Christoph Ilg
// Editiert:
//  Daniel Bauer
//
// Date:
//  24.03.2014
//------------------------------------------------------------

#ifndef PANORAMAKOPFCONTROLLER_CPP
#define PANORAMAKOPFCONTROLLER_CPP

#include "Panoramakopfcontroller.h"


unsigned char commandprocessor::commanddata[128];
unsigned char commandprocessor::commandlength;
unsigned char commandprocessor::command_nowindex;
unsigned char commandprocessor::command_lastindex;
unsigned char commandprocessor::machine_reg1;
unsigned char commandprocessor::machine_reg2;
unsigned char commandprocessor::machine_reg3;
unsigned char commandprocessor::machine_reg4;
short int commandprocessor::start_calc_offset;
int commandprocessor::offset;

unsigned char *bt_memptr;
short bt_memaddr;

#include <DueFlashStorage.h>
DueFlashStorage dueFlashStorage;


void commandprocessor::fillcontroller(unsigned char *dataptr, unsigned char datalength)
{
	command_nowindex = 0;
	machine_reg1 = 0;
	machine_reg2 = 0;
	machine_reg3 = 0;
	machine_reg4 = 0;
	if (datalength > 256) return;
	for (unsigned char i = 0; i < datalength; i++)
	{
		commanddata[i] = dataptr[i];
	}
	commandlength = datalength;
}

int commandprocessor::getOffset()
{
    int tmp = 0;
    // and read from flash like this:
    byte* b = dueFlashStorage.readAddress(4); // byte array which is read from flash at adress 4
    memcpy(&tmp, b, sizeof(tmp)); // copy byte array to temporary struct
    return tmp;
}

bool commandprocessor::process()
{
	while (command_nowindex < commandlength)
	{
		unsigned char nowcommand = commanddata[command_nowindex];
      
        if(command_lastindex != command_nowindex)
        {
#ifdef DEBUG_OUT
            Serial.println("---------------> Pano");
            Serial.print("-> Pano: Command Nr: ");
            Serial.print(command_nowindex);
            Serial.print(" von ");
            Serial.print(commandlength);
            Serial.print("\n");
        
            Serial.print("-> Pano: Nowcommand: ");
            Serial.print(nowcommand);
            Serial.print("\n");
#endif
            // Sende aktuellen Status an Handy
            bt_memaddr = mem::newadr(5);
            bt_memptr = mem::getptr(bt_memaddr);
            bt_memptr[0] = 3;
            bt_memptr[1] = PANORAMA_PROCESS_DATA;
            bt_memptr[2] = command_nowindex;
            bt_memptr[3] = commandlength;
            bt_memptr[4] = nowcommand;
            messager::put(bt_memaddr, CHANNELFORBT_TOSEND);
            command_lastindex = command_nowindex;
        }
        
        //Ende?
		if (nowcommand == 255) return true;
		//Warten
		if (nowcommand < 128)
		{
			Panoramakopfcontroller::c_wait(nowcommand);
			command_nowindex++;
			return false;
		}
		//Shift des Programmzeigers?
		if (nowcommand < 228)
		{
			command_nowindex = nowcommand-128;
			continue;
		}
		//Register setzen?
		if (nowcommand < 232)
		{
			if (nowcommand == 228) machine_reg1 = commanddata[command_nowindex + 1];
			if (nowcommand == 229) machine_reg2 = commanddata[command_nowindex + 1];
			if (nowcommand == 230) machine_reg3 = commanddata[command_nowindex + 1];
			if (nowcommand == 231) machine_reg4 = commanddata[command_nowindex + 1];
			command_nowindex += 2;
			continue;
		}
		//register um 1 dekrementieren, falls null erreicht wird, wird naechstes Befehlsbyte uebersprungen?
		if (nowcommand < 236)
		{
			command_nowindex++;
			if (nowcommand == 232)
			{
				machine_reg1--;
				if (machine_reg1 == 0) command_nowindex++;
				continue;
			}
			if (nowcommand == 233)
			{
				machine_reg2--;
				if (machine_reg2 == 0) command_nowindex++;
				continue;
			}
			if (nowcommand == 234)
			{
				machine_reg3--;
				if (machine_reg3 == 0) command_nowindex++;
				continue;
			}
			if (nowcommand == 235)
			{
				machine_reg4--;
				if (machine_reg4 == 0) command_nowindex++;
				continue;
			}
		}
		//Befehle an die Motoren?
		if (nowcommand < 241)
		{
			if (nowcommand == 236)
			{
				Panoramakopfcontroller::c_findzero();
				command_nowindex++;
				return false;
			}
			unsigned short data = ((unsigned short)commanddata[command_nowindex + 1]) * 256 + commanddata[command_nowindex + 2];
			command_nowindex += 3;
			if (nowcommand == 237)
            {
                Panoramakopfcontroller::c_motorNull(data, true);
                if(start_calc_offset == 1) offset += data;

            }
			if (nowcommand == 238)
            {
                Panoramakopfcontroller::c_motorNull(data, false);
                if(start_calc_offset == 1) offset -= data;
            }
			if (nowcommand == 239) Panoramakopfcontroller::c_motor(data, true);
			if (nowcommand == 240) Panoramakopfcontroller::c_motor(data, false);
			return false;
		}
		//loese Kamera aus
		if (nowcommand == 241) //241
		{
			if(!Panoramakopfcontroller::c_capture())
            {
                return false;
                //continue;
            }
            
		}
        //Fahre auf Sensor und beginne zaehlen
		if (nowcommand == 249) //249
		{
#ifdef DEBUG_OUT
            Serial.print("-> Pano: GoTo Zero, start count ");
            Serial.print("\n");
#endif
			offset = 0;
            byte b2[sizeof(offset)];
            memcpy(b2, &offset, sizeof(offset));
            dueFlashStorage.write(4, b2, sizeof(offset));
            start_calc_offset = 1;
		}
        //Uebernehme offset in EEPROM und stoppe zaehlen
		if (nowcommand == 243) //243
		{
            if(start_calc_offset == 1)
            //if(true)
            {
                
#ifdef DEBUG_OUT
                Serial.print("-> Pano: Stop count, add offset ");
                Serial.print("\n");
                Serial.print("-> Pano: VAL Offset: ");
                Serial.print(offset,DEC);
                Serial.print("\n");
#endif
                byte b2[sizeof(offset)];
                memcpy(b2, &offset, sizeof(offset));
                dueFlashStorage.write(4, b2, sizeof(offset));
                start_calc_offset = 0;
#ifdef DEBUG_OUT
                Serial.print("-> Pano: FLASH Offset: ");
                Serial.print(getOffset(),DEC);
                Serial.print("\n");
#endif
            }
        }
        //Fahre gespeicherten offset - getestet
        if (nowcommand == 244) //244
        {
            int step_count = getOffset();
            command_nowindex++;
            if(step_count > 0) Panoramakopfcontroller::c_motorNull(step_count, true);
            if(step_count < 0) {
                step_count *= -1;
                Panoramakopfcontroller::c_motorNull(step_count, false);
            }
            return false;
        }
		
		command_nowindex++;
	}

	return true;
}


//-------------------------------------------


bool Panoramakopfcontroller::workisdone;
unsigned char Panoramakopfcontroller::kommandotodo;
unsigned short Panoramakopfcontroller::motorstepstodo;
unsigned char Panoramakopfcontroller::steuerbefehl;
unsigned char Panoramakopfcontroller::wait_for_capture;
bool Panoramakopfcontroller::block_motors;

//eventfunktion als timer
void Panoramakopfcontroller::eventtimer()
{
	workisdone = true;
}

void Panoramakopfcontroller::init()
{
	//disable Motoren
	digitalWrite(PIN_M1_EN, HIGH);
	digitalWrite(PIN_M2_EN, HIGH);
	//setze variablenwerte
	workisdone = true;
	kommandotodo = 255;
	motorstepstodo = 0;
	steuerbefehl = 0;
    wait_for_capture = 0;
    block_motors = false;
}

void Panoramakopfcontroller::sendWorkStatus()
{
    // Sende aktuellen Status an Handy
    bt_memaddr = mem::newadr(3);
    bt_memptr = mem::getptr(bt_memaddr);
    bt_memptr[0] = 3;
    bt_memptr[1] = PANORAMA_WORK_DATA;
    if(steuerbefehl) bt_memptr[2] = 101;
    else bt_memptr[2] = 102;
    messager::put(bt_memaddr, CHANNELFORBT_TOSEND);
    
#ifdef DEBUG_OUT
    Serial.print("-> PTP: Send Work Info OK\n");
#endif
}


void Panoramakopfcontroller::run()
{
	//schaut nach, ob neues Programm da
	long message;
	if (messager::get(&message, CHANNELFORBT_PKOPFDATAIN))
	{
		unsigned char message_length = mem::getadr(message);
		unsigned char *message_ptr = mem::getptr(message);
		commandprocessor::fillcontroller(message_ptr, message_length);
		mem::freeadr(message);
	}
	//schaut an, ob Steuerbefehl da
	if (messager::get(&message, CHANNELFORBT_PKOPFCOMIN))
	{
		unsigned char message_length = mem::getadr(message);
		unsigned char *message_ptr = mem::getptr(message);
		if (message_length == 1)
		{
			steuerbefehl = *message_ptr;
		}
		mem::freeadr(message);
	}

	//schaut, ob steuerbefehl abzuarbeiten
	if (steuerbefehl == 1)
	{
        // LED on
        LEDtask::setRed(HIGH);
        
		// t�tigkeit schon ausgef�hrt?
		if (workisdone)
		{
            
			if (commandprocessor::process())
            {
                steuerbefehl = 0;
            }
		}
	}
	//stopbefehl
	if (steuerbefehl == 0)
	{
        // LED off
        LEDtask::setRed(LOW);
	}
}


void Panoramakopfcontroller::Motorrun()
{
	if (kommandotodo > 240) return;
	if (kommandotodo < 236) return;
	static bool valueM1 = true;
	static bool valueM2 = true;
	
    if(!block_motors)
    {
        //Nullposition?
        if (kommandotodo == 236)
        {
            digitalWrite(PIN_M1_CLK, valueM1);
            valueM1 = !valueM1;

            //Null erreicht?
            if (analogRead(PIN_POS_SIG) < PANORAMAKOPFNULLPOS_SCHWELLE)
            {
                //disable Positionssensor
                digitalWrite(PIN_POS_EN, HIGH);
                //disable Motor
                digitalWrite(PIN_M1_EN, HIGH);
                workisdone = true;
                //senke Geschwindigkeit
                short myadr = mem::newadr(6);
                unsigned char *myptr = mem::getptr(myadr);
                *myptr = 0;
                *(myptr + 1) = 1;
                *(myptr + 2) = 0;
                *(myptr + 3) = 0;
                *(myptr + 4) = 1;
                *(myptr + 5) = 144;
                messager::put(myadr, CHANNELFORSCHEDULER);
            }
        }
        else
        {
            if (kommandotodo == 239)
            {
                digitalWrite(PIN_M2_CLK, valueM2);
                valueM2 = !valueM2;
            }
            if (kommandotodo == 237)
            {
                digitalWrite(PIN_M1_CLK, valueM1);
                valueM1 = !valueM1;
            }
            motorstepstodo--;
            if (motorstepstodo == 0)
            {
                kommandotodo = 255;
                workisdone = true;
                //disable Motoren
                digitalWrite(PIN_M1_EN, HIGH);
                digitalWrite(PIN_M2_EN, HIGH);
            }
        }
    }
}


//leitet warten auf die eingegebene Zeit in 0,1 sec bevor workfinish ein
void Panoramakopfcontroller::c_wait(unsigned char time)
{
	eventsys::attach(EVENT_PKCONTROLLERTIMER, eventtimer);
	eventsys::raise_task(EVENT_PKCONTROLLERTIMER, 10*time);
	workisdone = false;
	//nutze eventtimer passend
}

//leitet finden der Nullposition ein
void Panoramakopfcontroller::c_findzero()
{
	//enable Positionssensor
	digitalWrite(PIN_POS_EN, HIGH);
	workisdone = false;
	kommandotodo = 236;
	//enable Motor
	digitalWrite(PIN_M1_EN, LOW);
    //richtung
    digitalWrite(PIN_M1_DIR,LOW);
	//erhoehe Geschwindigkeit
	short myadr = mem::newadr(6);
	unsigned char *myptr = mem::getptr(myadr);
	*myptr = 0;
	*(myptr + 1) = 1;
	*(myptr + 2) = 0;
	*(myptr + 3) = 0;
	*(myptr + 4) = 0;
	*(myptr + 5) = 125;
	messager::put(myadr, CHANNELFORSCHEDULER);
}

//dreht Motor mit Nullposition steps Schritte in direction
void Panoramakopfcontroller::c_motorNull(unsigned short steps, bool direction)
{
	workisdone = false;
	kommandotodo = 237;
	motorstepstodo = steps;
	//enable Motor
	digitalWrite(PIN_M1_EN, LOW);
	//setze direction
	digitalWrite(PIN_M1_DIR, direction);
}

//dreht anderen Motor steps Schritte in direction
void Panoramakopfcontroller::c_motor(unsigned short steps, bool direction)
{
	workisdone = false;
	kommandotodo = 239;
	motorstepstodo = steps;
	//enable Motor
	digitalWrite(PIN_M2_EN, LOW);
	//setze direction
	digitalWrite(PIN_M2_DIR, direction);
}

//loest die Bildaufnahem aus. Entweder Analog oder ueber USB
bool Panoramakopfcontroller::c_capture()
{
    wait_for_capture++;
    block_motors = true;
    
    if(wait_for_capture == PAUSE_BEFORE_CAPTURE)
    {
#ifdef DEBUG_OUT
        Serial.print("-> Pano: Capture ");
        Serial.print("\n");
#endif
        // Fokus Analog
        //digitalWrite(PIN_OPT_FOKUS, HIGH);
        //digitalWrite(PIN_OPT_FOKUS, LOW);
        
        // Shutter
        digitalWrite(PIN_OPT_SHUTTER,HIGH);
        
        // LED on
        digitalWrite(PIN_LED_STATUS, HIGH);
        LEDtask::setGreen(HIGH);
        
        // Ausl�ser Analog
        //digitalWrite(PIN_OPT_SHUTTER, LOW);
        //digitalWrite(PIN_OPT_SHUTTER,HIGH);
        
        // Ausl�ser USB
        PTPtask::capture();
        //delay(3);
        
        // LED off
        digitalWrite(PIN_LED_STATUS, LOW);
        LEDtask::setGreen(LOW);
        
        // Shutter
        digitalWrite(PIN_OPT_SHUTTER, LOW);

    }
    
    if(wait_for_capture >= (PAUSE_BEFORE_CAPTURE+PAUSE_AFTER_CAPTURE))
    {
        wait_for_capture = 0;
        
        block_motors = false;
        
        //digitalWrite(PIN_OPT_SHUTTER, LOW);
        
        return true;
    }
    
    return false;
}


#endif
