//------------------------------------------------------------
// Description:
//  PTP und USB Task
//
// Author:
//  Daniel Bauer
// Editiert:
//
//
// Date:
//  20.10.2014
//------------------------------------------------------------

#ifndef PTPTASK_H
#define PTPTASK_H

// Includes
#include <Arduino.h>
#include "../ourOS/functionality/scheduler.h"
#include "../ourOS/functionality/mem.h"
#include "../library/stringlib.h"
#include "protocoldefs.h"

#include <SPI.h>
#include <usbhub.h>

#include <ptp.h>
#include <canoneos.h>
#include "devinfoparser.h"
#include <ptpdebug.h>

class PTPtask
{
private:

public:
	// Initialisiert PTP und USB
	static void init();
	// Kontinuierlicher Aufruf von USB Task
	static void run();
    // Ruft den Auslöser auf
    static bool capture();
    // sendet die Kameradaten erneut
    static void sendCamInfo();
    //zeigt an, ob die Arbeit getan ist
	static bool ptp_connected;
    //cam stat befor
    static bool cam_stat_befor;
};

#endif
