//------------------------------------------------------------
// Description:
//  Der Controller f�r den Panoramakopf
//
// Author:
//  Christoph Ilg
// Editiert:
//  Daniel Bauer
//
// Date:
//  24.03.2014
//------------------------------------------------------------


#ifndef PANORAMAKOPFCONTROLLER_H
#define PANORAMAKOPFCONTROLLER_H

// Include
#include <Arduino.h>
#include "../ourOS/functionality/scheduler.h"
#include "../ourOS/functionality/mem.h"
#include "../ourOS/functionality/messager.h"
#include "../ourOS/functionality/eventsys.h"
#include "protocoldefs.h"
#include "PTPtask.h"

// Defines
#define PANORAMAKOPFNULLPOS_SCHWELLE 600
#define PAUSE_BEFORE_CAPTURE 120
#define PAUSE_AFTER_CAPTURE 120

class commandprocessor
{
private:
	static unsigned char commanddata[128];
	static unsigned char commandlength;
	static unsigned char command_nowindex;
    static unsigned char command_lastindex;
	static unsigned char machine_reg1;
	static unsigned char machine_reg2;
	static unsigned char machine_reg3;
	static unsigned char machine_reg4;
    static int offset;
    static short int start_calc_offset;
    
public:
	//kopiert daten aus Dataptr mit l�nge datalength in commanddata
	static void fillcontroller(unsigned char *dataptr, unsigned char datalength);
	//gibt genau dann true zur�ck, wenn finished, false wenn etwas zu tun bleibt
	static bool process();
    //gibt offset aus
    static int getOffset();
};

class Panoramakopfcontroller
{
private:
	//zeigt an, ob die Arbeit getan ist
	static bool workisdone;
	//gibt aktuellen steuerbefehl an
	static unsigned char steuerbefehl;
	//zeigt aktuelles Kommando an, das abgearbeitet wird
	static unsigned char kommandotodo;
	//gibt an, ob arbeit anzuhalten ist
	//
	//gibt an, wie viele Schritte der Motor noch zu drehen ist
	static unsigned short motorstepstodo;
	// Wie oft aufrufen bis ausl�sen
    static unsigned char wait_for_capture;
    static bool block_motors;
	//eventfunktion als timer
	static void eventtimer();
public:

	//Initialisiert den Panoramakopfcontroller
	static void init();
	//dient als Task, der ausgef�hrt wird um zu kontrollieren?
	static void run();
	//dient als Task f�r die Motoren, Taskzeit z.B. 500
	static void Motorrun();

	//leitet warten auf die eingegebene Zeit in 0,1 sec bevor workfinish ein
	static void c_wait(unsigned char time);
	//leitet finden der Nullposition ein
	static void c_findzero();
	//dreht Motor mit Nullposition steps Schritte in direction
	static void c_motorNull(unsigned short steps, bool direction);
	//dreht anderen Motor steps Schritte in direction
	static void c_motor(unsigned short steps, bool direction);
	//l�se Kamera aus, z.B. �ber USB - ist noch Dummy!
	static bool c_capture();
    //send workstatus
    static void sendWorkStatus();
};



#endif
