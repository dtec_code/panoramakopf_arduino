//------------------------------------------------------------
// Description:
//  Bluetooth functions
//
// Author:
//  Christoph Ilg
// Editiert:
//  Daniel Bauer
//
// Date:
//  24.03.2014
//------------------------------------------------------------


#ifndef BTTASK_H
#define BTTASK_H

// Includes
#include <Arduino.h>
#include "../ourOS/functionality/scheduler.h"
#include "../ourOS/functionality/mem.h"
#include "../library/stringlib.h"
#include "devinfoparser.h""

// Defines
#define TIMEOUT_MAXCTR 500 //bei 20ms Taskzeit alle 10s-> 500 (500)
#define TIMEFOR_ECHO 150 //gibt an, wie oft Echo gesendet wird -> 5s (250)
#define NAME_TO_CONNECT "Panoramakopf" //Name des Geraetes, zu dem Verbindung aufgebaut werden soll
#define PIN_FOR_CONNECTION "\r\n+RTPIN=1975\r\n" //Pin nach Anforderung
#define UARTTEMPLENGTH 128 //64
//#define DEVICE_TOSETUP

class BTtask
{
private:
	//ein OK wurde received
	static bool OK_received;
    //eine Aktion ist durchzufuehren
    static unsigned short doping;
	//counter um timeout zu erzielen
	static unsigned short timeout_CTR;
	//aktueller Empfangsstatus
	static unsigned char nowstate;
    //laenge der empfangenen Nachricht
    static unsigned char message_length;
    //zeiger auf die empfangene Nachricht
    static unsigned char *message_ptr;
    //Adresse zu verbinden
    static unsigned char address_datalength;
    static unsigned char address_data[18];
    //Name des Geraetes - zu verbinden
    static unsigned char name_datalength;
    static unsigned char name_data[20];
    
    static int last_devinfo;
    
	//Zwischenspeicheradresse
	static short UARTtempmem;
	static unsigned char *UARTtempmemptr;
	static unsigned char UARTwriteptr;

    //gibt an, ob die zweit strings zueinander passen
    static bool matching(unsigned char *instring, unsigned char inlength,
                             unsigned char *vglstring);
//echoreceived/send
	//Status 1: starte inquiring
	//static void do_phase1();
	/*//Status 2: warte auf gerat
	static void do_phase2();
	//Status 3: */

public:
	//zur Initialisierung aufrufen
	static void init();
	//BTtask zum aufrufen
	static void run();
    //gibt status zurueck
    static unsigned char state();
	//Task zur Abfrage des Serials
	static void serialtask();
};




#endif
