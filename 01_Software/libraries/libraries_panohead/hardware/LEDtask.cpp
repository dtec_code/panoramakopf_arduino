//------------------------------------------------------------
// Description:
//  LED functions
//
// Author:
//  Daniel Bauer
// Editiert:
//
//
// Date:
//  14.10.2014
//------------------------------------------------------------


#ifndef LEDTASK_CPP
#define LEDTASK_CPP

#include "LEDtask.h"


bool LEDtask::led_green;
bool LEDtask::led_red;
bool LEDtask::led_yellow;


//wird einmalig aufgerufen
void LEDtask::init()
{
    led_green == HIGH;
    led_red == HIGH;
    led_yellow == HIGH;
    
	digitalWrite(PIN_LED_BT, led_yellow);
    digitalWrite(PIN_LED_STATUS, led_green);
    digitalWrite(PIN_LED_WARN, led_red);
    
    led_green == LOW;
    led_red == LOW;
    led_yellow == LOW;
    
#ifdef DEBUG_OUT
    //Serial.print("-> LED: Init ");
    //Serial.print("\n");
#endif
    
}

//setze die LEDs
void LEDtask::setGreen(bool val)
{
    led_green = val;
}
void LEDtask::setRed(bool val)
{
    led_red = val;
}
void LEDtask::setYellow(bool val)
{
    led_yellow = val;
}

//kontinuierlicher Aufruf durch das Eventsystem - loop
void LEDtask::run()
{
    digitalWrite(PIN_LED_BT, led_yellow);
    digitalWrite(PIN_LED_STATUS, led_green);
    digitalWrite(PIN_LED_WARN, led_red);
}


#endif

