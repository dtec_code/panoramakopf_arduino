//------------------------------------------------------------
// Description:
//  PTP und USB Task
//
// Author:
//  Daniel Bauer
// Editiert:
//
//
// Date:
//  20.10.2014
//------------------------------------------------------------

#ifndef PTPTASK_CPP
#define PTPTASK_CPP

#include "PTPtask.h"

bool PTPtask::ptp_connected;
bool PTPtask::cam_stat_befor;

class CamStateHandlers : public PTPStateHandlers
{
    bool stateConnected;
    
public:
    CamStateHandlers() : stateConnected(false) {};
    
    virtual void OnDeviceDisconnectedState(PTP *ptp);
    virtual void OnDeviceInitializedState(PTP *ptp);
    
    bool connectionState() {return stateConnected;};
} CamStates;

USB         Usb;
USBHub      Hub1(&Usb);
CanonEOS    Eos(&Usb, &CamStates);

void CamStateHandlers::OnDeviceDisconnectedState(PTP *ptp)
{
    // disconnected
    if (stateConnected)
    {
        stateConnected = false;
        PTPtask::ptp_connected = false;
        //E_Notify(PSTR("Camera disconnected\r\n"),0x80);

#ifdef DEBUG_OUT
        Serial.print("-> PTP: Camera disconnected\n");
#endif
        // Sende aktuellen Status der Kamera an Handy
        bt_memaddr = mem::newadr(4);
        bt_memptr = mem::getptr(bt_memaddr);
        bt_memptr[0] = 3;
        bt_memptr[1] = CAMERA_STATUS_DATA;
        bt_memptr[2] = stateConnected;
        bt_memptr[3] = 0;
        messager::put(bt_memaddr, CHANNELFORBT_TOSEND);
    }
}

void CamStateHandlers::OnDeviceInitializedState(PTP *ptp)
{
    // connected
    if (!stateConnected)
    {
        stateConnected = true;
        PTPtask::ptp_connected = true;
        
#ifdef DEBUG_OUT
        Serial.print("-> PTP: Camera connected\n");
#endif
        // Sende aktuellen Status der Kamera an Handy
        bt_memaddr = mem::newadr(4);
        bt_memptr = mem::getptr(bt_memaddr);
        bt_memptr[0] = 3;
        bt_memptr[1] = CAMERA_STATUS_DATA;
        bt_memptr[2] = stateConnected;
        bt_memptr[3] = 0;
        messager::put(bt_memaddr, CHANNELFORBT_TOSEND);

        // Device Info
        uint16_t tmp;
        HexDump dump;
        tmp = Eos.GetDeviceInfo(&dump);
        
        DevInfoParser    prs;
        tmp = Eos.GetDeviceInfo(&prs);

#ifdef DEBUG_OUT
        E_Notify(PSTR("\n"), 0x80);
        Serial.print("-> PTP: Send Device Info \n");
#endif
        
        //tmp=Eos.GetDevicePropValue(1015, &prs);
        //E_Notify(PSTR("\n"), 0x80);
        
        // Lese Größe
        unsigned char *dat = prs.getDevInfoPoint();
        /*
        int dat_size = 0;
        while(*dat!=123 && dat_size <= 300)
        {
            //Serial.println(*dat);
            dat++;
            dat_size++;
        }
        
        // Send Device Info
        // Sende aktuellen Status der Kamera an Handy
        bt_memaddr = mem::newadr(dat_size+3);
        bt_memptr = mem::getptr(bt_memaddr);
        bt_memptr[0] = 3;
        bt_memptr[1] = CAMERA_DEVICE_DATA;
        bt_memptr[2] = stateConnected;
        
        //Serial.println(dat_size+3);
        // = +2
        
        unsigned char *dat_p = prs.getDevInfoPoint();
        for(int i = 3; i <= dat_size+2; i++)
        {
            bt_memptr[i] = *dat_p;
            //Serial.println(bt_memptr[i]);
            dat_p++;
            //Serial.println(i);
        }
        messager::put(bt_memaddr, CHANNELFORBT_TOSEND);
        
#ifdef DEBUG_OUT
        Serial.print("-> PTP: Send Device Info OK\n");
#endif
       */
    }
}

// Initialisiert PTP und USB
void PTPtask::init()
{
    cam_stat_befor = false;
    if (Usb.Init() == -1)
    {
#ifdef DEBUG_OUT
        Serial.println("-> PTP: OSC did not start");
#endif
    }
    else
    {
#ifdef DEBUG_OUT
    Serial.println("-> PTP: OSC started ");
#endif
    }
}

void PTPtask::sendCamInfo()
{
    if(true)
    //if(cam_stat_befor!=CamStates.connectionState())
    {
    // Sende aktuellen Status der Kamera an Handy
    bt_memaddr = mem::newadr(4);
    bt_memptr = mem::getptr(bt_memaddr);
    bt_memptr[0] = 3;
    bt_memptr[1] = CAMERA_STATUS_DATA;
    bt_memptr[2] = CamStates.connectionState();
    bt_memptr[3] = 0;
    messager::put(bt_memaddr, CHANNELFORBT_TOSEND);
    
    if(CamStates.connectionState())
    {
        // Lese Größe
        DevInfoParser    prs;
        unsigned char *dat = prs.getDevInfoPoint();
        int dat_size = 0;
        while(*dat!=123 && dat_size <= 300)
        {
            //Serial.println(*dat);
            dat++;
            dat_size++;
        }
    
        // Send Device Info
        // Sende aktuellen Status der Kamera an Handy
        bt_memaddr = mem::newadr(dat_size+3);
        bt_memptr = mem::getptr(bt_memaddr);
        bt_memptr[0] = 3;
        bt_memptr[1] = CAMERA_DEVICE_DATA;
        bt_memptr[2] = CamStates.connectionState();
        //Serial.println(CamStates.connectionState());
    
        //Serial.println(dat_size+3);
        // = +2
    
        unsigned char *dat_p = prs.getDevInfoPoint();
        for(int i = 3; i <= dat_size+2; i++)
        {
            bt_memptr[i] = *dat_p;
            //Serial.println(bt_memptr[i]);
            dat_p++;
            //Serial.println(i);
        }
        messager::put(bt_memaddr, CHANNELFORBT_TOSEND);
    }
#ifdef DEBUG_OUT
    Serial.print("-> PTP: Send Device Info OK\n");
#endif
    }
}

// Ruft den auslöser aus
bool PTPtask::capture()
{
    // Capture
    uint16_t rc = Eos.Capture();
    
    if (rc != PTP_RC_OK)
    {
        ErrorMessage<uint16_t>("Error", rc);
#ifdef DEBUG_OUT
        Serial.print("-> PTP: EOS Capture failure\n");
#endif
        // Sende aktuellen Status der Kamera an Handy
        bt_memaddr = mem::newadr(4);
        bt_memptr = mem::getptr(bt_memaddr);
        bt_memptr[0] = 3;
        bt_memptr[1] = CAMERA_STATUS_DATA;
        bt_memptr[2] = CamStates.connectionState();
        bt_memptr[3] = 2;
        messager::put(bt_memaddr, CHANNELFORBT_TOSEND);
        
        return false;
    }
    else
    {
#ifdef DEBUG_OUT
        Serial.print("-> PTP: EOS Capture ok\n");
#endif
        // Sende aktuellen Status der Kamera an Handy
        bt_memaddr = mem::newadr(4);
        bt_memptr = mem::getptr(bt_memaddr);
        bt_memptr[0] = 3;
        bt_memptr[1] = CAMERA_STATUS_DATA;
        bt_memptr[2] = CamStates.connectionState();
        bt_memptr[3] = 1;
        messager::put(bt_memaddr, CHANNELFORBT_TOSEND);
        
        return true;
    }
}

// Kontinuierlicher Aufruf von USB Task
void PTPtask::run()
{
    Usb.Task();
}

#endif