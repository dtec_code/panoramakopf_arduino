//------------------------------------------------------------
// Description:
//  LED functions
//
// Author:
//  Daniel Bauer
// Editiert:
//
//
// Date:
//  14.10.2014
//------------------------------------------------------------


#ifndef LEDTASK_H
#define LEDTASK_H

// Includes
#include <Arduino.h>
#include "../ourOS/functionality/scheduler.h"
#include "../ourOS/functionality/mem.h"
#include "../library/stringlib.h"
#include "pindefs.h"

class LEDtask
{
private:
	//LEDs
	static bool led_green;
    static bool led_red;
    static bool led_yellow;
 
public:
	//zur Initialisierung aufrufen
	static void init();
	//LEDtask zum aufrufen
	static void run();
    //setzte die LEDs
    static void setGreen(bool val);
    static void setRed(bool val);
    static void setYellow(bool val);
};

#endif
