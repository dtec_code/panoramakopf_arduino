//------------------------------------------------------------
// Description:
//  Bluetooth functions
//
// Author:
//  Christoph Ilg
// Editiert:
//  Daniel Bauer
//
// Date:
//  24.03.2014
//------------------------------------------------------------


#ifndef BTTASK_CPP
#define BTTASK_CPP

#include "BTtask.h"


bool BTtask::OK_received;
unsigned short BTtask::doping;
unsigned short BTtask::timeout_CTR;
unsigned char BTtask::nowstate;
unsigned char BTtask::message_length;
unsigned char *BTtask::message_ptr;
unsigned char BTtask::address_datalength;
unsigned char BTtask::address_data[18];
unsigned char BTtask::name_datalength;
unsigned char BTtask::name_data[20];
int BTtask::last_devinfo;

//Zwischenspeicheradresse
short BTtask::UARTtempmem;
unsigned char *BTtask::UARTtempmemptr;
unsigned char BTtask::UARTwriteptr;
int echo_rcv_counter;


#ifdef DEBUG_OUT
    //Zwischenspeicher - Vorherhiger Zustand
    int further_nowstate;
    int echo_send_counter;
#endif

//wird einmalig aufgerufen
void BTtask::init()
{
	nowstate = 0;
	timeout_CTR = 0;
	OK_received = false;
    doping = 0;
    address_data[18] = '\0';
    last_devinfo = -8;
    echo_rcv_counter = 0;
    echo_send_counter = 0;

#ifdef DEBUG_OUT
    further_nowstate = 10;
#endif
    
	//initialisiere Zwischenspeicheradresse fuer UARTempfang
	UARTwriteptr = 0;
	UARTtempmem = mem::newadr(UARTTEMPLENGTH);
	UARTtempmemptr = mem::getptr(UARTtempmem);
    
#ifdef DEVICE_TOSETUP
        //Geruest auf Master +STWMOD=1
        //Pin setzen +STPIN=1975
        Serial3.print("\r\n+STAUTO=1\r\n");
#endif
}

//kontinuierlicher Aufruf durch das Eventsystem - loop
void BTtask::run()
{
        //timeout counter
        timeout_CTR++;
        if ((nowstate > 2)||(nowstate==1))
        {
            if (timeout_CTR >= TIMEOUT_MAXCTR) nowstate = 0;
        }
        else 
        {
            if (timeout_CTR >= (3*TIMEOUT_MAXCTR)) nowstate = 0;
        }

#ifdef DEBUG_OUT
    if(further_nowstate!=nowstate)
    {
        Serial.println("---------------> BT");
        Serial.print("-> BT: Nowstate: ");
        Serial.print(nowstate);
        further_nowstate=nowstate;
        Serial.print("\n");
    }
#endif
        //echo counter
        if (nowstate==4)
        {
            doping++;
            if (doping >= TIMEFOR_ECHO)
            {
#ifdef DEBUG_OUT
                Serial.print("-> BT: send Echo ");
                Serial.print(echo_send_counter);
                Serial.print(" -> LED LOW\n");
#endif
                echo_send_counter++;
                doping = 0;
				Serial3.print("\r\necho\r\n"); // r n
                
                // LED off
                LEDtask::setYellow(LOW);
                
            }
        }

        //Nachricht empfangen - ist was da?
        long message;
        if (messager::get(&message,CHANNELFORUART)&&nowstate!=0)
        {
            
            message_length = mem::getadr(message);
            message_ptr = mem::getptr(message);
            
            //Echo Nachricht wurde empfangen
            if (matching(message_ptr,4,(unsigned char*)"echo"))
            {
#ifdef DEBUG_OUT
                Serial.print("-> BT: rcv Echo ");
                Serial.print(echo_rcv_counter);
                Serial.print(" -> LED HIGH\n");
#endif
                echo_rcv_counter++;
                // LED on
                LEDtask::setYellow(HIGH);
                nowstate = 4;
                timeout_CTR = 0;
                mem::freeadr(message);
                
                if(echo_rcv_counter == last_devinfo+1) Panoramakopfcontroller::sendWorkStatus();
                if(echo_rcv_counter >= last_devinfo+5)
                {
                    PTPtask::sendCamInfo();
                    last_devinfo = echo_rcv_counter;
                }
                
                    
                return;
            }
            
            //Nachricht zum weiterleiten wurde empfangen, muss mit ! beginnen
            if (nowstate==4)
            {
                if (*message_ptr == '!')
                {
#ifdef DEBUG_OUT
                    Serial.println("-> BT: rcv Command: ");
#endif
					//pruefe, ob Nachricht gueltig ist
					if (!stringlib::unpackage_BT_message(message_ptr, message_length))
                    {
#ifdef DEBUG_OUT
                        Serial.print("-> BT: Msg: not valid \n");
#endif
                        return;
                    }
                    else
                    {
#ifdef DEBUG_OUT
                        Serial.print("-> BT: Msg: valid \n");
#endif
                    }
					unsigned char *ptr = stringlib::getbufferptr();

                    //sende Nachricht an entsprechendes Postfach
					short memaddr = mem::newadr(ptr[3]);
					unsigned char *memptr = mem::getptr(memaddr);
					for (unsigned char i = 0; i < ptr[3]; i++)
					{
						memptr[i] = ptr[i + 4];
#ifdef DEBUG_OUT
                      //  Serial.print(memptr[i]);
#endif
					}
					messager::put(memaddr, ptr[2]);
#ifdef DEBUG_OUT
                    //Serial.print("\n");
#endif
                }
                timeout_CTR=0;
                mem::freeadr(message);
                return;
            }
            
            //Error wurde empfangen
            if (matching(message_ptr,5,(unsigned char*)"ERROR"))
            {
#ifdef DEBUG_OUT
                Serial.print("-> BT: rcv Error \n");
#endif
                nowstate = 0;
                mem::freeadr(message);
                return;
            }
            
            //BTstate wurde empfangen
            if (matching(message_ptr,9,(unsigned char*)"+BTSTATE:"))
            {
                nowstate = *(message_ptr+9)-'0';
                timeout_CTR = 0;
                mem::freeadr(message);
#ifdef DEBUG_OUT
                // 0: Initializing, 1: Ready, 2: Inquiring, 3: Connecting, 4: Connected
                Serial.print("-> BT: rcv +BTSTATE: ");
                Serial.print(nowstate);
                Serial.print("\n");
#endif
                return;
            }
            
            //OK wurde empfangen
            if (matching(message_ptr,2,(unsigned char*)"OK"))
            {
#ifdef DEBUG_OUT
                Serial.print("-> BT: rcv OK \n");
#endif
                OK_received = true;
                timeout_CTR = 0;
                mem::freeadr(message);                
                return;
            }
            
            //RTINQ wurd empfangen
            if (matching(message_ptr,7,(unsigned char*)"+RTINQ="))
            {
                timeout_CTR = 0;
                for (address_datalength = 0;address_datalength<17;address_datalength++)
                {
                    unsigned char data = *(message_ptr+address_datalength+7);
                    if (data==';') break;
                    address_data[address_datalength] = data;
                }
                address_datalength++;
                name_datalength = message_length - 7 - address_datalength;
                if (name_datalength > 20) name_datalength = 20;
                for (unsigned char i = 0;i<name_datalength;i++)
                {
                    name_data[i] = *(message_ptr+address_datalength+7+i);
                }
                
                //pruefe, ob der Name des Geraetes passend, wenn ja verbinde
                if (matching(name_data,name_datalength,(unsigned char*)NAME_TO_CONNECT))
                {
                    if (address_datalength < 17) address_data[address_datalength] = '\0';
					//Serial3.print("\r\n+CONN=")+address_data+"\r\n");  <- ist ja slave, nicht noetig!!
                }
                timeout_CTR = 0;
                mem::freeadr(message);
#ifdef DEBUG_OUT
                Serial.print("-> BT: rcv +RTINQ= \n");
#endif
                return;
            }
            
            //Pin-Anforderung wurde empfangen
            if (matching(message_ptr,6,(unsigned char*)"+INPIN"))
            {
#ifdef DEBUG_OUT
                Serial.print("-> BT: rcv +INPIN= \n");
#endif
				Serial3.print(PIN_FOR_CONNECTION);
                timeout_CTR = 0;
                mem::freeadr(message);
                return;
            }
            mem::freeadr(message);
            
        }
        else if (nowstate == 0)
        {
#ifdef DEBUG_OUT
            Serial.print("-> BT: send +INQ=1\n");
            echo_send_counter = 0;
#endif
            echo_rcv_counter = 0;
            last_devinfo = -8;
            //sende INQ nachricht
            Serial3.print("\r\n+INQ=1\r\n");
            nowstate = 1;
            timeout_CTR = 0;
            // LED on
            LEDtask::setYellow(HIGH);
            
        }

		//ist eine Nachricht zu senden?
		if (nowstate == 4)
		{
			long message2;
			if (messager::get(&message2, CHANNELFORBT_TOSEND))
			{

				unsigned char message_length2 = mem::getadr(message2);
				unsigned char *message_ptr2 = mem::getptr(message2);
				//verpacke fuer versand, erstes unsigned char ist der Zielkanal
				stringlib::package_BT_message(&(message_ptr2[1]), message_length2 - 1, message_ptr2[0]);
				mem::freeadr(message2);
				//versende 
				Serial3.print("\r\n");
				Serial3.print((const char*)stringlib::getbufferptr());
				Serial3.print("\r\n");
#ifdef DEBUG_OUT
                Serial.print("-> BT send Command: ");
                Serial.print((const char*)stringlib::getbufferptr());
                Serial.print("\n");
#endif
                
            }
		}

}


bool BTtask::matching(unsigned char *instring, unsigned char inlength, unsigned char *vglstring)
{
    for (unsigned char i = 0; i < inlength; i++)
    {
        if (*(instring+i) != *(vglstring+i)) return false;
    }
    if (*(vglstring+inlength)!='\0') return false;
    return true;
}


unsigned char BTtask::state()
{
    return nowstate;
}


void BTtask::serialtask()
{
	while (Serial3.available() > 0)
	{
		unsigned char received = Serial3.read();
		if ((received != '\r'))
		{
			if (received == '\n')
			{
				if (UARTwriteptr != 0)
				{
					//schicke ab
					mem::shortenlength(UARTtempmem, UARTwriteptr);
					messager::put(UARTtempmem, CHANNELFORUART);
					//hole neuen Speicherbereich
					UARTwriteptr = 0;
					UARTtempmem = mem::newadr(UARTTEMPLENGTH);
					UARTtempmemptr = mem::getptr(UARTtempmem);
				}
			}
			else
			{
				*(UARTtempmemptr + UARTwriteptr) = received;
				UARTwriteptr++;
				if (UARTwriteptr == UARTTEMPLENGTH) UARTwriteptr = 0;
			}
		}
	}
}


#endif
