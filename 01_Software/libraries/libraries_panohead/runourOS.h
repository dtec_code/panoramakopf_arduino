//------------------------------------------------------------
// Description:
//  Das Interface zum starten der Software
//
// Author:
//  Christoph Ilg
// Editiert:
//  Daniel Bauer
//
// Date:
//  24.03.2014
//------------------------------------------------------------


#ifndef RUNOUROS_H
#define RUNOUROS_H

// Include
#include <Arduino.h>
#include "ourOS/functionality/scheduler.h"
#include "ourOS/functionality/mem.h"
#include "ourOS/functionality/messager.h"
#include "ourOS/functionality/eventsys.h"

#include "hardware/pindefs.h"
#include "hardware/PTPtask.h"
#include "hardware/Panoramakopfcontroller.h"
#include "hardware/BTtask.h"
#include "hardware/LEDtask.h"

// Define
#define DEBUG_OUT // ifdef debug

void writegetcommand(String command)
{
  digitalWrite(PIN_BT_RTS,LOW);
  delay(10);
  while (digitalRead(PIN_BT_CTS)==HIGH) delay(10);
  Serial3.print("\r\n"+command+"\r\n");
  delay(10);
  digitalWrite(PIN_BT_RTS,HIGH);  
}



//startet die Software - loop
void runourOS()
{

    //Debugtest
	Serial.begin(9600);
	Serial.println("Hello PC from Arduino");
	delay(1000);
	
    //Pins initialisieren
    //Optokoppler Kameraverbindung
	pinMode(PIN_OPT_SHUTTER, OUTPUT);
    pinMode(PIN_OPT_FOKUS, OUTPUT);
    digitalWrite(PIN_OPT_SHUTTER, LOW);
    digitalWrite(PIN_OPT_FOKUS, LOW);

    //LEDs
	pinMode(PIN_LED_STATUS, OUTPUT);
	pinMode(PIN_LED_BT, OUTPUT);
	pinMode(PIN_LED_WARN, OUTPUT);
	digitalWrite(PIN_LED_STATUS, LOW);
	digitalWrite(PIN_LED_BT, LOW);
	digitalWrite(PIN_LED_WARN, LOW);
	
    //Motor 1
	pinMode(PIN_M1_EN, OUTPUT);
	pinMode(PIN_M1_DIR, OUTPUT);
	pinMode(PIN_M1_CLK, OUTPUT);
	pinMode(PIN_M1_ABSENK, INPUT);
	digitalWrite(PIN_M1_EN, HIGH);
	digitalWrite(PIN_M1_DIR, LOW);
	digitalWrite(PIN_M1_CLK, LOW);
	
    //Motor 2
	pinMode(PIN_M2_EN, OUTPUT);
	pinMode(PIN_M2_DIR, OUTPUT);
	pinMode(PIN_M2_CLK, OUTPUT);
	pinMode(PIN_M2_ABSENK, INPUT);
	digitalWrite(PIN_M2_EN, HIGH);
	digitalWrite(PIN_M2_DIR, LOW);
	digitalWrite(PIN_M2_CLK, LOW);
	
    //Positionssensor
	pinMode(PIN_POS_EN, OUTPUT);
	digitalWrite(PIN_POS_EN, LOW);
	
    //Bluetooth
	Serial3.begin(38400);
	Serial3.flush();
  	
    //Standardpinpegel BT setzen
  	pinMode(PIN_BT_CTS,INPUT); //CTS geht rein
  	pinMode(PIN_BT_RTS,INPUT); //RTS geht rein
    
    // --> Debug
#ifdef DEBUG_OUT
    Serial.println("-> Initialisiere Pins");
#endif
	//initialisiere messager
	messager::messagerinit();
    // --> Debug
#ifdef DEBUG_OUT
    Serial.println("-> Initialisiere Messa");
#endif
	//initialisiere mem
	mem::meminit();
    // --> Debug
#ifdef DEBUG_OUT
    Serial.println("-> Initialisiere Mem");
#endif
	//initialisiere eventsys
	eventsys::init();
    // --> Debug
#ifdef DEBUG_OUT
    Serial.println("-> Initialisiere Events");
#endif
    //initialisiere Panoramakopfcontroller
    Panoramakopfcontroller::init();
    // --> Debug
#ifdef DEBUG_OUT
    Serial.println("-> Initialisiere Pano");
#endif
	//initialisere Bluetooth
	BTtask::init();
    // --> Debug
#ifdef DEBUG_OUT
    Serial.println("-> Initialisiere BT");
#endif
    //initialisere PTP
	PTPtask::init();
    // --> Debug
#ifdef DEBUG_OUT
    Serial.println("-> Initialisiere PTP");
#endif
    //initialisere LED
	LEDtask::init();
    // --> Debug
#ifdef DEBUG_OUT
    Serial.println("-> Initialisiere LED");
#endif
	//starte scheduler!!!
	Scheduler::run();
    // --> Debug
#ifdef DEBUG_OUT
    Serial.println("-> Start Sched");
#endif
}

//Dies ist bei Arduino notwendig wenn Dateien in Unterordner
#include "ourOS/functionality/messager.cpp"
#include "ourOS/functionality/mem.cpp"
#include "ourOS/functionality/scheduler.cpp"
#include "ourOS/functionality/eventsys.cpp"

#include "hardware/BTtask.cpp"
#include "hardware/Panoramakopfcontroller.cpp"
#include "library/stringlib.cpp"
#include "hardware/LEDtask.cpp"
#include "hardware/PTPtask.cpp"

#endif
