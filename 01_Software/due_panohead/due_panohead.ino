/*
Sobald der runourOS-Ordner in den libraries-Ordner kopiert wurde,
kann diese Datei zum hochladen auf das Due-Board benutzt werden!
*/

// Include
#include <runourOS.h>
#include <SPI.h>
#include <usbhub.h>
#include <ptp.h>
#include <canoneos.h>
#include <ptpdebug.h>
#include <DueFlashStorage.h>

// Einmaliger Aufruf - loop in runourOS()
void setup()
{ 
  // Systemstart
  delay(2000);
  runourOS();  
}

// Kontinuierlicher Aufruf - Nicht verwendet, da loop in runourOS()
void loop()
{
 
}

